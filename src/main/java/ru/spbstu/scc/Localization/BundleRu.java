package ru.spbstu.scc.Localization;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
public class BundleRu extends AbstractBundle {
    private static BundleRu instance;

    private BundleRu(Locale locale) {
        super(locale);
    }

    @Bean(name = "bundleRu")
    public static BundleRu getInstance(){
        if (instance != null)
            return instance;
        synchronized (BundleRu.class){
            if (instance != null)
                return instance;
            instance = new BundleRu(new Locale("ru"));
            return instance;
        }
    }
}

package ru.spbstu.scc.Localization;

import java.util.Locale;
import java.util.ResourceBundle;

public abstract class AbstractBundle implements Bundle {
    private ResourceBundle bundle;

    public AbstractBundle(Locale locale) {
        this.bundle = ResourceBundle.getBundle("localization", locale);
    }

    @Override
    public String getString(String key) {
        return bundle.getString(key);
    }
}

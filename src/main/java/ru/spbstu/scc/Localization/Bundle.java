package ru.spbstu.scc.Localization;

public interface Bundle {
    String getString(String key);
}

package ru.spbstu.scc.Localization;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
public class BundleEn extends AbstractBundle {
    private static BundleEn instance;

    private BundleEn(Locale locale) {
        super(locale);
    }

    @Bean(name = "bundleEn")
    public static BundleEn getInstance(){
        if (instance != null)
            return instance;
        synchronized (BundleEn.class){
            if (instance != null)
                return instance;
            instance = new BundleEn(new Locale("en"));
            return instance;
        }
    }
}

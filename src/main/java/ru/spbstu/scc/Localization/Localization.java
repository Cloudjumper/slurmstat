package ru.spbstu.scc.Localization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component
@Scope(scopeName = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class Localization {
    private final Bundle bundleRu;
    private final Bundle bundleEn;
    private Bundle defaultBundle;

    @Autowired
    public Localization(@Qualifier("bundleEn") Bundle bundleEn, @Qualifier("bundleRu") Bundle bundleRu) {
        this.bundleEn = bundleEn;
        this.bundleRu = bundleRu;
        defaultBundle = this.bundleRu;
    }

    public String get(String key){
        return defaultBundle.getString(key);
    }

    public void changeLocalization(String lang){
        Language language = Language.valueOf(lang.toUpperCase());
        switch (language){
            case EN:{
                this.defaultBundle = this.bundleEn;
                break;
            }
            case RU:{
                this.defaultBundle = this.bundleRu;
                break;
            }
            default:{
                this.defaultBundle = this.bundleRu;
                break;
            }
        }
    }
}

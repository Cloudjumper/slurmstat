package ru.spbstu.scc.utils;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 * Created by cloudjumper on 2/20/17.
 */
public class LocalDateTimeConverter {
    public static long toUnix(LocalDateTime time){
        return time.toEpochSecond(ZoneOffset.of("+03:00:00"));
    }
}

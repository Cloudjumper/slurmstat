package ru.spbstu.scc.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by cloudjumper on 1/25/17.
 */
public class UserId {
    private static final Logger LOGGER = LogManager.getLogger("FASTD");
    public static int getId(String user) throws IOException {
        try {
            Process process = Runtime.getRuntime().exec("id -u " + user);
            try(InputStreamReader procReader = new InputStreamReader(process.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(procReader)) {
                return Integer.parseInt(bufferedReader.readLine());
            }
        } catch (IOException e){
            LOGGER.error("Error create process");
            throw e;
        } catch (NumberFormatException e){
            LOGGER.error("Error read process result or can not get id");
            return -1;
        }
    }
}

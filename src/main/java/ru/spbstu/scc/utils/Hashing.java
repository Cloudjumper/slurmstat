package ru.spbstu.scc.utils;

import org.mindrot.jbcrypt.BCrypt;

import java.util.ResourceBundle;

/**
 * Created by cloudjumper on 3/1/17.
 */
public class Hashing {
    private static final String SALT = ResourceBundle.getBundle("hashing").getString("salt");
    public static String hash(String pass){
        return BCrypt.hashpw(pass, SALT).replace(SALT,"");
    }

    public static boolean check(String pass, String hash){
        return BCrypt.checkpw(pass, SALT + hash);
    }
}

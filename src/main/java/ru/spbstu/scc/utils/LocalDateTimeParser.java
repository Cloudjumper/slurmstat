package ru.spbstu.scc.utils;



import ru.spbstu.scc.exceptions.LocalDateTimeParseException;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * Created by cloudjumper on 2/1/17.
 */
public interface LocalDateTimeParser {

    DateTimeFormatter[] patterns = {
            DateTimeFormatter.ofPattern("yyyy MM dd HH mm"),
            DateTimeFormatter.ofPattern("dd MM yyyy HH mm"),
            DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm"),
            DateTimeFormatter.ofPattern("y-M-d-H-m"),
            DateTimeFormatter.ofPattern("y/M/d-H:m"),
            DateTimeFormatter.ISO_DATE_TIME
    };
    static LocalDateTime parse(String date) throws LocalDateTimeParseException {
        for (DateTimeFormatter d: patterns) {
            try{
                return LocalDateTime.parse(date, d);
            } catch (NullPointerException e){
                throw new LocalDateTimeParseException("null");
            } catch (DateTimeParseException ignored){}

        }
        throw new LocalDateTimeParseException("Parse error");
    }
}

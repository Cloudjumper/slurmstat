package ru.spbstu.scc.services.special;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import ru.spbstu.scc.db.slurm.dao.AcctTableDAO;
import ru.spbstu.scc.db.slurm.dao.UserTableDAO;
import ru.spbstu.scc.db.slurmacc.dao.SlurmAccDAO;
import ru.spbstu.scc.db.slurmacc.dao.SlurmUserDAO;
import ru.spbstu.scc.db.slurmacc.entities.SlurmUser;
import ru.spbstu.scc.services.slurm.AcctTableService;

import java.io.IOException;
import java.sql.SQLException;

public class CrossDbService {

    @Autowired
    @Qualifier("FASTD")
    private Logger logger;

    @Autowired
    @Qualifier("acctTableDAO")
    private AcctTableDAO acctTableDAO;

    @Autowired
    @Qualifier("userTableDAO")
    private UserTableDAO userTableDAO;

    @Autowired
    @Qualifier("slurmAccDAO")
    private SlurmAccDAO slurmAccDAO;

    @Autowired
    @Qualifier("slurmUserDAO")
    private SlurmUserDAO slurmUserDAO;

    private static CrossDbService instance;

    private CrossDbService(){}

    public static CrossDbService getInstance(){
        if (instance != null)
            return instance;
        synchronized (CrossDbService.class){
            if (instance != null)
                return instance;
            instance = new CrossDbService();
            return instance;
        }
    }

    public void updateTables(){
        try {
            for (String account: acctTableDAO.getAccounts()){
                slurmAccDAO.addOrUpdateUser(account);
                logger.debug(account);
            }
            for (SlurmUser user: userTableDAO.getNamesWithId()){
                slurmUserDAO.addOrUpdateUser(user);
                logger.debug(user);
            }
        } catch (SQLException | IOException e) {
            logger.error(e.getMessage());
        }
    }

}

package ru.spbstu.scc.services.slurm;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

public class AcctTableService {
    private static volatile AcctTableService instance;

    private AcctTableService(){}

    @Bean("acctTableService")
    public static AcctTableService getInstance(){
        if (instance != null)
            return instance;
        synchronized (AcctTableService.class){
            if (instance != null)
                return instance;
            instance = new AcctTableService();
            return instance;
        }
    }
}

package ru.spbstu.scc.services.slurmacc;

import ru.spbstu.scc.db.slurmacc.dao.SlurmAccDAO;
import ru.spbstu.scc.exceptions.ToControllerException;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class SlurmAccService {
    private static volatile SlurmAccService instance;

    private static SlurmAccDAO slurmAccDAO;

    private SlurmAccService() {
    }

    public static SlurmAccService getInstance(SlurmAccDAO slurmAccDAO){
        if (instance != null)
            return instance;
        synchronized (SlurmAccService.class){
            if (instance != null)
                return instance;
            instance = new SlurmAccService();
            SlurmAccService.slurmAccDAO = slurmAccDAO;
            return instance;
        }
    }

    public List<String> getAccounts() throws ToControllerException {
        try {
            return slurmAccDAO.getAccounts();
        } catch (SQLException | IOException e) {
            throw new ToControllerException(e.getMessage());
        }
    }

    public void addOrUpdateUser(String account) throws ToControllerException {
        try {
            slurmAccDAO.addOrUpdateUser(account);
        } catch (SQLException e) {
            throw new ToControllerException(e.getMessage());
        }
    }
}

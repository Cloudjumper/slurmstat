package ru.spbstu.scc.controllers.api;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.spbstu.scc.Localization.Localization;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Locale;

/**
 * Created by cloudjumper on 4/21/17.
 */
@Controller
public class LangApi {
    private final Localization localization;

    @Autowired
    public LangApi(@Qualifier("localization") Localization localization) {
        this.localization = localization;
    }

    @RequestMapping(path = "/api/lang", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<String> langChange(HttpSession httpSession, @RequestParam String lang) {
        localization.changeLocalization(lang);
        if (httpSession.getAttribute("lang").equals(lang)){
            return new ResponseEntity<>("{\"response\": \"OK\"}", HttpStatus.NOT_MODIFIED);
        }
        if (lang.equals("ru")){
            httpSession.setAttribute("lang", "ru");
            httpSession.setAttribute("locale", new Locale("ru"));
            return new ResponseEntity<>("{\"response\": \"OK\"}", HttpStatus.OK);
        }
        if (lang.equals("en")){
            httpSession.setAttribute("lang", "en");
            httpSession.setAttribute("locale", new Locale("en"));
            return new ResponseEntity<>("{\"response\": \"OK\"}", HttpStatus.OK);
        }
        return new ResponseEntity<>("{\"error\": \"Lang change error\"}", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(path = "/api/lang", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<String> jsLocalization() {
        try (InputStream inputStream = LangApi.class.getResourceAsStream("/localizationJS.json")) {
            JSONObject jsonObject = new JSONObject(new JSONTokener(inputStream));
            return new ResponseEntity<>(jsonObject.toString(), HttpStatus.OK);
        } catch (IOException e) {
            return new ResponseEntity<>("\"error\": \"Localization error\"", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

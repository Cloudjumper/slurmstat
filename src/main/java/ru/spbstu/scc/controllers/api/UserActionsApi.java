package ru.spbstu.scc.controllers.api;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.spbstu.scc.db.slurmacc.dao.UserDAO;
import ru.spbstu.scc.db.slurmacc.entities.User;
import ru.spbstu.scc.exceptions.NoSuchUserException;
import ru.spbstu.scc.utils.Hashing;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@Controller
public class UserActionsApi {
    @Autowired
    @Qualifier("FASTD")
    private Logger logger;

    @Autowired
    @Qualifier("userDAO")
    private UserDAO userDAO;

    @RequestMapping(path = "/api/user", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    protected ResponseEntity<String> addOrEditUser(HttpSession httpSession,
                                                   @RequestParam(name = "login", required = true) String loginParam,
                                                   @RequestParam(name = "pass", required = true) String passParam,
                                                   @RequestParam(name = "userId", required = true) String userIdParam,
                                                   @RequestParam(name = "accountId", required = true) String accountIdParam,
                                                   @RequestParam(name = "user", required = true) String userParam,
                                                   @RequestParam(name = "account", required = true) String accountParam,
                                                   @RequestParam(name = "role", required = true) String roleParam)
            throws ServletException, IOException {
        User sUser = (User) httpSession.getAttribute("sUser");
        if (sUser.getRoleCode() != 0)
            return new ResponseEntity<>("{\"error\": \"Access denied\"}", HttpStatus.FORBIDDEN);
        try {
            User user = new User();
            user.setLogin(loginParam);
            if (passParam.equals(""))
                user.setPass(userDAO.getUser(loginParam).getPass());
            else
                user.setPass(Hashing.hash(passParam));
            if (!userIdParam.equals(""))
                user.setUserId(Integer.parseInt(userIdParam));
            else
                user.setUserId(-1);
            if (!accountIdParam.equals(""))
                user.setAccountId(Integer.parseInt(accountIdParam));
            else
                user.setAccountId(-1);
            if (!userParam.equals(""))
                user.setUser(userParam);
            if (!accountParam.equals(""))
                user.setAccount(accountParam);
            if (Integer.parseInt(roleParam) > 3 || Integer.parseInt(roleParam) < 0)
                user.setRoleCode(3);
            else
                user.setRoleCode(Integer.parseInt(roleParam));
            userDAO.addOrUpdateUser(user);
            return new ResponseEntity<>("{\"response\": \"OK\"}", HttpStatus.OK);
        } catch (SQLException | NoSuchUserException e) {
            logger.error(e.getMessage());
            return new ResponseEntity<>("{\"error\": \"Something went wrong\"}", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (NumberFormatException e){
            logger.error(e.getMessage());
            return new ResponseEntity<>("{\"error\": \"Wrong input data\"}", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(path = "/api/user", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    protected ResponseEntity<String> getUser(HttpSession httpSession,
                           @RequestParam(name = "login", required = true) String loginParam)
            throws ServletException, IOException {
        User sUser = (User) httpSession.getAttribute("sUser");
        if (sUser.getRoleCode() != 0)
            return new ResponseEntity<>("", HttpStatus.FORBIDDEN);
        try {
            User user = userDAO.getUser(loginParam);
            return new ResponseEntity<>(user.toJsonString(), HttpStatus.OK);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return new ResponseEntity<>("{\"error\": \"Something went wrong\"}", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (NoSuchUserException e) {
            logger.error(e.getMessage());
            return new ResponseEntity<>("{\"error\": \"No such user\"}", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(path = "/api/user", method = RequestMethod.DELETE, produces = "application/json;charset=UTF-8")
    @ResponseBody
    protected ResponseEntity<String> deleteUser(HttpSession httpSession,
                              @RequestParam(name = "login", required = true) String loginParam)
            throws ServletException, IOException {
        User sUser = (User) httpSession.getAttribute("sUser");
        if (sUser.getRoleCode()!= 0)
            return new ResponseEntity<>("", HttpStatus.FORBIDDEN);
        try {
            userDAO.deleteUser(loginParam);
            return new ResponseEntity<>("{\"response\": \"OK\"}", HttpStatus.OK);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return new ResponseEntity<>("{\"error\": \"Something goes wrong!\"}", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (NoSuchUserException e) {
            logger.error(e.getMessage());
            return new ResponseEntity<>("{\"error\": \"" + e.getMessage() + "\"}", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

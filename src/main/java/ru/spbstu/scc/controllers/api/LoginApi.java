package ru.spbstu.scc.controllers.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.spbstu.scc.session.SessionData;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Login in {@link ru.spbstu.scc.configs.SpringSecurityConfiguration}
 */
@Controller
public class LoginApi {

//    @Autowired
//    @Qualifier("sessionData")
//    private SessionData sessionData;

    @RequestMapping(value="/logout", method = RequestMethod.DELETE, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<String> logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
//        sessionData.clear();
        return new ResponseEntity<>("{\"response\": \"/\"}", HttpStatus.OK);//You can redirect wherever you want, but generally it's a good idea to show login screen again.
    }
}

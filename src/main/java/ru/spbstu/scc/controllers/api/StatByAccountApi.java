package ru.spbstu.scc.controllers.api;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.spbstu.scc.db.slurm.dao.RscJobTableDAO;
import ru.spbstu.scc.db.slurm.entities.RscJobTable;
import ru.spbstu.scc.db.slurmacc.entities.User;
import ru.spbstu.scc.exceptions.LocalDateTimeParseException;
import ru.spbstu.scc.logic.RscJobTableStatisticCollector;
import ru.spbstu.scc.utils.LocalDateTimeConverter;
import ru.spbstu.scc.utils.LocalDateTimeParser;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@Controller
public class StatByAccountApi {
    @Autowired
    @Qualifier("FASTD")
    private Logger logger;

    @Autowired
    @Qualifier("rscJobTableDAO")
    private RscJobTableDAO rscJobTableDAO;

    @RequestMapping(path = "/api/stat/account", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    protected ResponseEntity<String> getStatistic(HttpSession httpSession,
                                   @RequestParam(name = "account", required = true) String accountParam,
                                   @RequestParam(name = "sTime", required = true) String sTimeParam,
                                   @RequestParam(name = "eTime", required = true) String eTimeParam)
            throws ServletException, IOException {
        try {
            User sUser = (User) httpSession.getAttribute("sUser");
            switch (sUser.getRoleCode()) {
                case 0:
                case 1: {
                    long sTime = LocalDateTimeConverter.toUnix(LocalDateTimeParser.parse(sTimeParam));
                    long eTime = LocalDateTimeConverter.toUnix(LocalDateTimeParser.parse(eTimeParam));
                    List<RscJobTable> res = rscJobTableDAO.getByAccount(accountParam, sTime, eTime);
                    return new ResponseEntity<>(RscJobTableStatisticCollector.groupResultToJsonStr(res, sTime, eTime),
                            HttpStatus.OK);
                }
                case 2: {
                    long sTime = LocalDateTimeConverter.toUnix(LocalDateTimeParser.parse(sTimeParam));
                    long eTime = LocalDateTimeConverter.toUnix(LocalDateTimeParser.parse(eTimeParam));
                    List<RscJobTable> res = rscJobTableDAO.getByAccount(sUser.getAccount(), sTime, eTime);
                    return new ResponseEntity<>(RscJobTableStatisticCollector.groupResultToJsonStr(res, sTime, eTime),
                            HttpStatus.OK);
                }
                case 3: {
                    break;
                }
            }
            return new ResponseEntity<>("How did you get there?", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (LocalDateTimeParseException e) {
            logger.error(e.getMessage());
            return new ResponseEntity<>("Wrong date format!", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return new ResponseEntity<>("Wrong input data!", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

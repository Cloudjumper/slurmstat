package ru.spbstu.scc.controllers.page;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.spbstu.scc.Localization.Localization;
import ru.spbstu.scc.session.SessionData;

import javax.servlet.http.HttpSession;
import java.util.Locale;

@Controller
public class Pages {

    @Autowired
    @Qualifier("FASTD")
    private Logger logger;

//    @Autowired
//    @Qualifier("sessionData")
//    private SessionData sessionData;

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView index (){
        return new ModelAndView("index");
    }

    @RequestMapping(path = "/e/500", method = RequestMethod.GET)
    public ModelAndView page500(HttpSession httpSession,
                                @RequestParam(name = "msg", required = false) String msg,
                                ModelMap modelMap){
        ModelAndView view = new ModelAndView("500");
//        if (sessionData.getUser() != null)
//            view.addObject("user", sessionData.getRole().toString().toLowerCase());
        if (msg == null || msg.equals("")) {
            return view;
        }
        view.addObject("msg", msg);
        return view;
    }

    @RequestMapping(path = "/e/400", method = RequestMethod.GET)
    public ModelAndView page400(HttpSession httpSession,
                                @RequestParam(name = "msg", required = false) String msg,
                                ModelMap modelMap){
        ModelAndView view = new ModelAndView("400");
        if (httpSession.getAttribute("sUser") != null)
            view.addObject("User", httpSession.getAttribute("sUser"));
        if (msg == null || msg.equals(""))
            return view;
        view.addObject("msg", msg);
        return view;
    }
}

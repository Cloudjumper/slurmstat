package ru.spbstu.scc.controllers.page;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LeadPage {

    @RequestMapping(path = "/lead", method = RequestMethod.GET)
    public ModelAndView lead(){
        return new ModelAndView("lead");
    }
}

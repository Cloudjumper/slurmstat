package ru.spbstu.scc.controllers.page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.spbstu.scc.db.ConnectionPool;
import ru.spbstu.scc.db.slurmacc.dao.PartitionDAO;
import ru.spbstu.scc.db.slurmacc.dao.SlurmAccDAO;
import ru.spbstu.scc.db.slurmacc.dao.SlurmUserDAO;
import ru.spbstu.scc.db.slurmacc.dao.UserDAO;

import java.io.IOException;
import java.sql.SQLException;

@Controller
public class AdminPage {
    @Autowired
    @Qualifier("userDAO")
    private UserDAO userDAO;

    @Autowired
    @Qualifier("slurmUserDAO")
    private SlurmUserDAO slurmUserDAO;

    @Autowired
    @Qualifier("slurmAccDAO")
    private SlurmAccDAO slurmAccDAO;

    @Autowired
    @Qualifier("partitionDAO")
    private PartitionDAO partitionDAO;

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public ModelAndView admin() throws SQLException, IOException {
        ModelAndView  view = new ModelAndView("admin");
        view.addObject("all", userDAO.getAll());
        view.addObject("slurmUsers", slurmUserDAO.getNamesWithId());
        view.addObject("slurmAccounts", slurmAccDAO.getAccounts());
        view.addObject("slurmPartitions", partitionDAO.getPartitions());
        return view;
    }
}

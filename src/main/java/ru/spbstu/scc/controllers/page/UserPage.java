package ru.spbstu.scc.controllers.page;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserPage {

    @RequestMapping(path = "/user", method = RequestMethod.GET)
    public ModelAndView user(){
        return new ModelAndView("user");
    }
}

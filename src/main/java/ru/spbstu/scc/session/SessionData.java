package ru.spbstu.scc.session;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import ru.spbstu.scc.Localization.Localization;
import ru.spbstu.scc.configs.security.Role;
import ru.spbstu.scc.db.slurmacc.entities.User;

@Component
@Scope(value = WebApplicationContext.SCOPE_SESSION)
@Getter
@Setter
public class SessionData {
    private User user;
    private Role role;

    public SessionData() {
    }

    public void clear(){
        user = null;
        role = null;
    }

    public boolean isUserExist(){
        return user != null;
    }
}

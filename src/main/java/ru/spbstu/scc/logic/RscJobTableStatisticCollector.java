package ru.spbstu.scc.logic;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import ru.spbstu.scc.db.slurm.entities.RscJobTable;
import ru.spbstu.scc.db.slurmacc.dao.SlurmUserDAO;
import ru.spbstu.scc.db.slurmacc.pool.SlurmAccConnectionPool;

import java.sql.SQLException;
import java.util.*;

public class RscJobTableStatisticCollector {
    private static final Logger LOGGER = LogManager.getLogger("FASTD");

    public static String groupResultToJsonStr(List<RscJobTable> in, long sTime, long eTime) throws SQLException {
        return groupResult(in, sTime, eTime).toString();
    }

    public static JSONObject groupResultToJsonObj(List<RscJobTable> in, long sTime, long eTime) throws SQLException {
        return groupResult(in, sTime, eTime);
    }

    private static JSONObject groupResult(List<RscJobTable> in, long sTime, long eTime) throws SQLException {
        //Calculate work time in nodes per hour
        List<ModifiedRscJobTable> modIn = new ArrayList<>();
        for (RscJobTable item : in) {
            modIn.add(new ModifiedRscJobTable(
                    item.getAccount(),
                    item.getUserId(),
                    item.getPartition(),
                    (((item.getTimeEnd() > eTime ? eTime : item.getTimeEnd()) -
                            (item.getTimeStart() < sTime ? sTime : item.getTimeStart())) /
                            3600d) * item.getNodesAlloc()));

        }
        //Sum all duplicated work time, create unique records from all
        Map<String, Map<Integer, Map<String, Double>>> map = new HashMap<>();
        for (ModifiedRscJobTable item : modIn) {
            if (!map.containsKey(item.getAccount()))
                map.put(item.getAccount(), new HashMap<>());

            if (!map.get(item.getAccount()).containsKey(item.getUserId()))
                map.get(item.getAccount()).put(item.getUserId(), new HashMap<>());

            if (!map.get(item.getAccount()).get(item.getUserId()).containsKey(item.getPartition())) {
                map.get(item.getAccount()).get(item.getUserId()).put(item.getPartition(), item.getTimeWork());
            } else {
                Double tpm = map.get(item.getAccount()).get(item.getUserId()).get(item.getPartition());
                map.get(item.getAccount()).get(item.getUserId()).put(item.getPartition(), item.getTimeWork() + tpm);
            }
        }
        // Create JSON correct string
        JSONObject root = new JSONObject();
        Map<String, Double> total = new HashMap<>();
        Map<Integer, String> idUser = new HashMap<>();
        Set<String> k1 = map.keySet(); // Accounts
        for (String k11 : k1) {
            Set<Integer> k2 = map.get(k11).keySet(); // UserIds
            for (Integer k22 : k2) {
                Set<String> k3 = map.get(k11).get(k22).keySet(); // Partitions
                for (String k33 : k3) {
                    if (!idUser.containsKey(k22))
                        //TODO Bad
                        idUser.put(k22, SlurmUserDAO.getInstance(SlurmAccConnectionPool.getInstance()).getUserById(k22));
                    root.append("client", new JSONObject()
                            .put("account", k11)
                            .put("userId", idUser.get(k22))
                            .put("partition", k33)
                            .put("workTime", String.format("%1$.2f", map.get(k11).get(k22).get(k33))));
                    total.put(k33, total.containsKey(k33) ?
                            total.get(k33) + map.get(k11).get(k22).get(k33) :
                            map.get(k11).get(k22).get(k33));
                }
            }
        }
        JSONObject partitions = new JSONObject();
        for (String part : total.keySet()) {
            partitions.put(part, String.format("%1$.2f", total.get(part)));
        }
        root.put("total", partitions);
        return root;
    }
}

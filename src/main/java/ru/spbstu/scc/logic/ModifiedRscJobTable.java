package ru.spbstu.scc.logic;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import org.json.JSONObject;

@AllArgsConstructor
@ToString
@Getter
class ModifiedRscJobTable {
    private String account;
    private int userId;
    private String partition;
    private Double timeWork;

    public String toJsonString(){
        return toJsonObj().toString();
    }

    public JSONObject toJsonObj(){
        JSONObject json = new JSONObject();
        json.put("account", account);
        json.put("userId", userId);
        json.put("partition", partition);
        json.put("timeWork", timeWork);
        return json;
    }
}

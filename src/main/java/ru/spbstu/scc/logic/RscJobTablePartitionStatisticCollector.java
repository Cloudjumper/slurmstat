package ru.spbstu.scc.logic;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import ru.spbstu.scc.db.slurm.entities.RscJobTable;
import ru.spbstu.scc.db.slurmacc.dao.PartitionDAO;
import ru.spbstu.scc.db.slurmacc.pool.SlurmAccConnectionPool;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RscJobTablePartitionStatisticCollector {
    private static final Logger LOGGER = LogManager.getLogger("FASTD");

    public static String groupResultToJsonStr(List<RscJobTable> in, long sTime, long eTime) throws SQLException {
        return groupResult(in, sTime, eTime).toString();
    }

    public static JSONObject groupResultToJsonObj(List<RscJobTable> in, long sTime, long eTime) throws SQLException {
        return groupResult(in, sTime, eTime);
    }

    @SuppressWarnings("Duplicates")
    private static JSONObject groupResult(List<RscJobTable> in, long sTime, long eTime) throws SQLException {
        //TODO if 0 collection?
        String partition = in.get(0).getPartition();
        //Calculate work time in nodes per hour
        List<ModifiedRscJobTable> modIn = new ArrayList<>();
        BigDecimal delTime; //eTime - sTime
        BigDecimal n; //nodes
        for (RscJobTable item : in) {
            // check item time for being in range sTime and eTime, if not - cut
            delTime = new BigDecimal(
                    Long.toString(
                            ((item.getTimeEnd() > eTime ? eTime : item.getTimeEnd()) -
                                    (item.getTimeStart() < sTime ? sTime : item.getTimeStart()))
                    )
            );
            n = new BigDecimal(Long.toString(item.getNodesAlloc()));
            modIn.add(new ModifiedRscJobTable(
                    item.getAccount(),
                    item.getUserId(),
                    item.getPartition(),
                    delTime.divide(BigDecimal.valueOf(3600), 7, BigDecimal.ROUND_HALF_UP).multiply(n).doubleValue()
            ));

        }
        //Sum all duplicated work time, create unique records from all
        BigDecimal sum = BigDecimal.valueOf(0); // sum all work time for partition
        Map<String, BigDecimal> map = new HashMap<>();
        for (ModifiedRscJobTable item : modIn) {
            if (map.containsKey(item.getAccount())) {
                map.put(item.getAccount(), map.get(item.getAccount()).add(BigDecimal.valueOf(item.getTimeWork())));
            }
            else {
                map.put(item.getAccount(), BigDecimal.valueOf(item.getTimeWork()));
            }
            sum = sum.add(BigDecimal.valueOf(item.getTimeWork()));
        }

        // Create JSON correct string
        JSONObject root = new JSONObject();
        double pwt;
        double wt;
        for (String k : map.keySet()){
            pwt = map.get(k)
                    .divide(sum, 7, BigDecimal.ROUND_HALF_UP)
                    .multiply(BigDecimal.valueOf(100))
                    .setScale(2, RoundingMode.HALF_UP)
                    .doubleValue();
            wt = map.get(k)
                    .setScale(2, RoundingMode.HALF_UP)
                    .doubleValue();
            root.append("client", new JSONObject()
                    .put("account", k)
                    .put("workTime", wt)
                    .put("partWorkTime", pwt));
        }

        //TODO Bad
        int nodes = PartitionDAO.getInstance(SlurmAccConnectionPool.getInstance()).getNodes(partition);
        double hours = (eTime - sTime)/3600d;
        BigDecimal l = sum.divide(BigDecimal.valueOf(hours)
                .multiply(BigDecimal.valueOf(nodes)), 7, BigDecimal.ROUND_HALF_UP);
        JSONObject total = new JSONObject();
        total.put("sumWorkTime", sum.setScale(2, BigDecimal.ROUND_HALF_UP));
        total.put("load", l.setScale(2, RoundingMode.HALF_UP));
        total.put("maxWorkTime", BigDecimal.valueOf(hours*(double) nodes).setScale(2, RoundingMode.HALF_UP));
        root.put("total", total);

        return root;
    }
}

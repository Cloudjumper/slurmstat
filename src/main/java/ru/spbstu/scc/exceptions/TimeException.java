package ru.spbstu.scc.exceptions;

/**
 * Created by cloudjumper on 1/30/17.
 */
public class TimeException extends Exception {
    public TimeException() {
        super();
    }

    public TimeException(String message) {
        super(message);
    }

    public TimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public TimeException(Throwable cause) {
        super(cause);
    }

    protected TimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

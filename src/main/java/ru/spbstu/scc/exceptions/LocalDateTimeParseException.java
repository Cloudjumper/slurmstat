package ru.spbstu.scc.exceptions;

/**
 * Created by cloudjumper on 2/1/17.
 */
public class LocalDateTimeParseException extends Exception {
    public LocalDateTimeParseException(String message) {
        super(message);
    }

    public LocalDateTimeParseException() {
        super();
    }

    public LocalDateTimeParseException(String message, Throwable cause) {
        super(message, cause);
    }

    public LocalDateTimeParseException(Throwable cause) {
        super(cause);
    }

    protected LocalDateTimeParseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

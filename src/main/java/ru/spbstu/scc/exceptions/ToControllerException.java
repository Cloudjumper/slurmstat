package ru.spbstu.scc.exceptions;

/**
 * Created by cloudjumper on 4/23/17.
 */
public class ToControllerException extends Exception {
    public ToControllerException() {
        super();
    }

    public ToControllerException(String message) {
        super(message);
    }

    public ToControllerException(String message, Throwable cause) {
        super(message, cause);
    }

    public ToControllerException(Throwable cause) {
        super(cause);
    }

    protected ToControllerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

package ru.spbstu.scc.exceptions;

/**
 * Created by cloudjumper on 2/18/17.
 */
public class LoginCheckException extends Exception {
    public LoginCheckException() {
        super();
    }

    public LoginCheckException(String message) {
        super(message);
    }

    public LoginCheckException(String message, Throwable cause) {
        super(message, cause);
    }

    public LoginCheckException(Throwable cause) {
        super(cause);
    }

    protected LoginCheckException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

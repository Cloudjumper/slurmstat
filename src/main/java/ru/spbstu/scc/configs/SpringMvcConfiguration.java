package ru.spbstu.scc.configs;

import org.springframework.context.annotation.*;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import ru.spbstu.scc.session.SessionData;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "ru.spbstu.scc.*")
public class SpringMvcConfiguration extends WebMvcConfigurerAdapter {

	@Bean
	@Scope(value = WebApplicationContext.SCOPE_SESSION)
	public SessionData sessionData(){
		return new SessionData();
	}

	@Bean
	public InternalResourceViewResolver internalResourceViewResolver(){
		InternalResourceViewResolver internalResourceViewResolver = new InternalResourceViewResolver();
		internalResourceViewResolver.setPrefix("/WEB-INF/view/");
		internalResourceViewResolver.setSuffix(".jsp");
//		internalResourceViewResolver.setContentType("charset=UTF-8");
		return internalResourceViewResolver;
	}

	/*
     * Configure ResourceHandlers to serve static resources like CSS/ Javascript etc...
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("/static/");
    }
}
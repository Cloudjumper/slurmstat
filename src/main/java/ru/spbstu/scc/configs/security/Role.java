package ru.spbstu.scc.configs.security;

/**
 * Created by cloudjumper on 4/24/17.
 */
public enum Role {
    USER("3"), LEAD("2"), SEER("1"), ADMIN("0");

    private final String role;

    Role(String role) {
        this.role = role;
    }

    public String getRole(){
        return this.role;
    }
    public int getIntRole(){
        return Integer.valueOf(this.role);
    }

}

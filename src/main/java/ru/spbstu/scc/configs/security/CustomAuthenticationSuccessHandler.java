package ru.spbstu.scc.configs.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import ru.spbstu.scc.db.slurmacc.dao.UserDAO;
import ru.spbstu.scc.db.slurmacc.entities.User;
import ru.spbstu.scc.exceptions.NoSuchUserException;
import ru.spbstu.scc.services.special.CrossDbService;
import ru.spbstu.scc.session.SessionData;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

@Component
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    private final CrossDbService crossDbService;
    private final UserDAO userDAO;
//    @Autowired
//    @Qualifier("sessionData")
//    private SessionData sessionData;

    @Autowired
    public CustomAuthenticationSuccessHandler(@Qualifier("userDAO") UserDAO userDAO,
                                              @Qualifier("crossDbService") CrossDbService crossDbService) {
        this.userDAO = userDAO;
        this.crossDbService = crossDbService;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest,
                                        HttpServletResponse httpServletResponse,
                                        Authentication authentication) throws IOException, ServletException {
        try {
            User user = userDAO.getUser(authentication.getName());
            httpServletRequest.getSession().setAttribute("sUser", user);
//            sessionData.setUser(user);

            String redirect;
            switch (((List<GrantedAuthority>) authentication.getAuthorities())
                    .stream()
                    .findFirst()
                    .orElse(new CustomGrantedAuthority("-1"))
                    .getAuthority()){
                case "0": {
                    redirect = "/admin";
                    crossDbService.updateTables();
//                    sessionData.setRole(Role.ADMIN);
                    break;
                }
                case "1": {
                    redirect = "/seer";
                    crossDbService.updateTables();
//                    sessionData.setRole(Role.SEER);
                    break;}
                case "2": {
                    redirect = "/lead";
//                    sessionData.setRole(Role.LEAD);
                    break;
                }
                case "3": {
                    redirect = "/user";
//                    sessionData.setRole(Role.USER);
                    break;
                }
                default: {redirect = "/"; break;}
            }
//            new DefaultRedirectStrategy().sendRedirect(httpServletRequest, httpServletResponse, redirect);

            httpServletResponse.setStatus(200);
            try(PrintWriter writer = httpServletResponse.getWriter()) {
                writer.println(String.format("{%1$s: %2$s}",
                        "\"redirect\"",
                        "\"" + redirect + "\""));
            }
        } catch (SQLException | NoSuchUserException e) {
            e.printStackTrace();
            new DefaultRedirectStrategy().sendRedirect(httpServletRequest, httpServletResponse, "/e/404");
        }
    }
}

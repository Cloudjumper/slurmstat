package ru.spbstu.scc.configs.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ru.spbstu.scc.db.slurmacc.dao.UserDAO;
import ru.spbstu.scc.db.slurmacc.entities.User;
import ru.spbstu.scc.exceptions.NoSuchUserException;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

@Component
public class CustomUserDetailsService implements UserDetailsService {

    private final UserDAO userDAO;

    // Put user credentials from a DB, RemeberMe,..
    private static final GrantedAuthority AUTHORITY_ADMIN = new CustomGrantedAuthority(Role.ADMIN.getRole());
    private static final GrantedAuthority AUTHORITY_SEER = new CustomGrantedAuthority(Role.SEER.getRole());
    private static final GrantedAuthority AUTHORITY_LEAD = new CustomGrantedAuthority(Role.LEAD.getRole());
    private static final GrantedAuthority AUTHORITY_USER = new CustomGrantedAuthority(Role.USER.getRole());

    @Autowired
    public CustomUserDetailsService(@Qualifier("userDAO") UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            User user = userDAO.getUser(username);
            if (user == null) {
                throw new UsernameNotFoundException("Username or password incorrect!");
            }
            Set<GrantedAuthority> authorities = new HashSet<>();
            switch (Integer.toString(user.getRoleCode())){
                case "0": {authorities.add(AUTHORITY_ADMIN); break;}
                case "1": {authorities.add(AUTHORITY_SEER); break;}
                case "2": {authorities.add(AUTHORITY_LEAD); break;}
                case "3": {authorities.add(AUTHORITY_USER); break;}
            }
            return new CustomUserDetails(user.getLogin(), user.getPass(), authorities);
        } catch (SQLException | NoSuchUserException e) {
            throw new UsernameNotFoundException("Username or password incorrect!");
        }
    }
}

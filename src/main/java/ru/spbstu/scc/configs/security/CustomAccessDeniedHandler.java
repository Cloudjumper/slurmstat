package ru.spbstu.scc.configs.security;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Component
public class CustomAccessDeniedHandler implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest httpServletRequest, 
                       HttpServletResponse httpServletResponse, 
                       AccessDeniedException e) throws IOException, ServletException {
        try(PrintWriter writer = httpServletResponse.getWriter()) {
            httpServletResponse.setStatus(404);
            writer.println("{\"error\": \"" + e.getMessage() + "\"}");
        }
    }
}

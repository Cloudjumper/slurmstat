package ru.spbstu.scc.configs.security;

import org.springframework.security.core.GrantedAuthority;

public class CustomGrantedAuthority implements GrantedAuthority {

	private String role;

	public CustomGrantedAuthority(String role) {
		this.role = role;
	}

	public String getAuthority() {
		return role;
	}
}
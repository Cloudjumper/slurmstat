package ru.spbstu.scc.configs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfiguration extends WebSecurityConfigurerAdapter {
    private final AuthenticationProvider authenticationProvider;
    private final AuthenticationFailureHandler authenticationFailureHandler;
    private final AuthenticationSuccessHandler authenticationSuccessHandler;
    private final AccessDeniedHandler accessDeniedHandler;

    @Autowired
    public SpringSecurityConfiguration(
            @Qualifier("customAuthenticationProvider") AuthenticationProvider authenticationProvider,
            @Qualifier("customAuthenticationFailureHandler") AuthenticationFailureHandler authenticationFailureHandler,
            @Qualifier("customAuthenticationSuccessHandler") AuthenticationSuccessHandler authenticationSuccessHandler, @Qualifier("customAccessDeniedHandler") AccessDeniedHandler accessDeniedHandler) {
        this.authenticationProvider = authenticationProvider;
        this.authenticationFailureHandler = authenticationFailureHandler;
        this.authenticationSuccessHandler = authenticationSuccessHandler;
        this.accessDeniedHandler = accessDeniedHandler;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/", "/static/**", "/logout", "/api/lang", "/e/*")
                .permitAll()
                .antMatchers("/api/**", "/api/*")
                .authenticated()
                .antMatchers("/user").hasAuthority("3")
                .antMatchers("/lead").hasAuthority("2")
                .antMatchers("/seer").hasAuthority("1")
                .antMatchers("/admin").hasAuthority("0")
//                .access("hasRole('ROLE_USER')")
                .and()
                    .formLogin()
                        .loginPage("/")
                        .loginProcessingUrl("/login")
                        .failureHandler(authenticationFailureHandler)
                        .usernameParameter("username")
                        .passwordParameter("password")
                        .successHandler(authenticationSuccessHandler)
                .and()
                    .exceptionHandling()
                        .accessDeniedHandler(accessDeniedHandler)
                .and()
                    .authenticationProvider(authenticationProvider);
    }

    private SavedRequestAwareAuthenticationSuccessHandler savedRequestAwareAuthenticationSuccessHandler() {
        SavedRequestAwareAuthenticationSuccessHandler auth = new SavedRequestAwareAuthenticationSuccessHandler();
        auth.setTargetUrlParameter("targetUrl");
        return auth;
    }

//    private AuthenticationFailureHandler authenticationFailureHandler(){
//        AuthenticationFailureHandler handler = (httpServletRequest, httpServletResponse, e) -> {
//
//        };
//        return handler;
//    }

//    private AuthenticationSuccessHandler authenticationSuccessHandler(){
//        AuthenticationSuccessHandler handler = (httpServletRequest, httpServletResponse, authentication) -> {
//
//        };
//        return handler;
//    }

}
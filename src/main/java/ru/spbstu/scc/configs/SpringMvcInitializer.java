package ru.spbstu.scc.configs;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import ru.spbstu.scc.filter.CommonFilter;

import javax.servlet.Filter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

public class SpringMvcInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

//	@Override
//	public void onStartup(ServletContext servletContext) throws ServletException {
//		AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
//		context.register(SpringMvcConfiguration.class);
//		servletContext.addListener(new ContextLoaderListener(context));
//	}
//
//	private AnnotationConfigWebApplicationContext getContext() {
//		AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
//		context.setConfigLocation();
//		return context;
//	}

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] { SpringMvcConfiguration.class };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class<?>[]{SpringMvcConfiguration.class};
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}

/*	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		servletContext.addFilter("CommonFilter", CommonFilter.class);
		super.onStartup(servletContext);
	}*/

	@Override
	protected Filter[] getServletFilters() {
		return new Filter[]{new CommonFilter()};
	}

//	@Override
//	protected void registerContextLoaderListener(ServletContext servletContext) {
//
//	}
}

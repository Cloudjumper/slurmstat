package ru.spbstu.scc.db.slurmacc.dao;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import ru.spbstu.scc.db.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PartitionDAO {

    //language=MySQL
    private static final String SELECT_NODES = "SELECT nodes FROM `Partition` WHERE `partition`=?";
    private static final String SELECT_PARTITIONS = "SELECT `partition` FROM `Partition`";

    private static ConnectionPool connectionPool;
    private static volatile PartitionDAO instance;

    private PartitionDAO(){
    }

    public static PartitionDAO getInstance(@Qualifier("slurmAccConnectionPool") ConnectionPool connectionPool){
        if (instance != null)
            return instance;
        synchronized (PartitionDAO.class){
            if (instance != null)
                return instance;
            instance = new PartitionDAO();
            PartitionDAO.connectionPool = connectionPool;
            return instance;
        }
    }

    public int getNodes(String partition) throws SQLException {
        try(Connection connection = connectionPool.getConnection();
            PreparedStatement ps = connection.prepareStatement(SELECT_NODES)) {
            ps.setString(1, partition);
            try(ResultSet rs = ps.executeQuery()) {
                if (rs.next())
                    return rs.getInt("nodes");
                return -1;
            }
        }
    }

    public List<String> getPartitions() throws SQLException {
        try(Connection connection = connectionPool.getConnection();
            PreparedStatement ps = connection.prepareStatement(SELECT_PARTITIONS);
            ResultSet rs = ps.executeQuery()) {
            List<String> res = new ArrayList<>();
            while (rs.next())
                res.add(rs.getString("partition"));
            return res;
        }
    }
}

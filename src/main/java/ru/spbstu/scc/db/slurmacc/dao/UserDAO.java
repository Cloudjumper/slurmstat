package ru.spbstu.scc.db.slurmacc.dao;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import ru.spbstu.scc.db.ConnectionPool;
import ru.spbstu.scc.db.slurmacc.entities.User;
import ru.spbstu.scc.exceptions.NoSuchUserException;
import ru.spbstu.scc.db.slurmacc.pool.SlurmAccConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDAO {
    @SuppressWarnings("unused")
    @Autowired
    @Qualifier("FASTD")
    private Logger logger;
    //language=MySQL
    private static final String SELECT_ALL = "SELECT login, user_id, account_id, user, account, role_code FROM User ORDER BY role_code, login";
    //language=MySQL
    private static final String SELECT_USER = "SELECT * FROM User WHERE login=?";
    //language=MySQL
    private static final String SELECT_USER_ID = "SELECT DISTINCT user_id FROM User WHERE user=?";
    //language=MySQL
    private static final String ADD_UPDATE_USER = "INSERT INTO User " +
            "(login, pass, user_id, account_id, user, `account`, role_code) " +
            "VALUES (?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE " +
            "pass = VALUES(pass), user_id = VALUES(user_id), account_id = VALUES(account_id)," +
            "user = VALUES(user), `account` = VALUES(`account`), role_code = VALUES(role_code)";
    //language=MySQL
    private static final String DELETE_USER = "DELETE FROM User WHERE login=?";

    private static final String LOG_IN = "SELECT count(id) as 'count' FROM User WHERE User.login=? AND User.pass=?;";

    private static ConnectionPool connectionPool;
    private static volatile UserDAO instance;

    private UserDAO(){
    }

    public static UserDAO getInstance(ConnectionPool connectionPool){
        if (instance != null)
            return instance;
        synchronized (UserDAO.class){
            if (instance != null)
                return instance;
            instance = new UserDAO();
            UserDAO.connectionPool = connectionPool;
            return instance;
        }
    }

//    For admin page
    @SuppressWarnings("Duplicates")
    public List<User> getAll() throws SQLException {
        try(Connection connection = connectionPool.getConnection();
            PreparedStatement ps = connection.prepareStatement(SELECT_ALL);
            ResultSet rs = ps.executeQuery()
        ) {
            List<User> all = new ArrayList<>();
            while (rs.next()){
                all.add(User.builder()
                        .login(rs.getString("login"))
                        .user(rs.getString("user"))
                        .account(rs.getString("account"))
                        .userId(rs.getInt("user_id"))
                        .accountId(rs.getInt("account_id"))
                        .roleCode(rs.getInt("role_code"))
                        .build());
            }
            return all;
        }
    }

    @SuppressWarnings("Duplicates")
    public User getUser(String login) throws SQLException, NoSuchUserException {
        try(Connection connection = connectionPool.getConnection();
            PreparedStatement ps = connection.prepareStatement(SELECT_USER)) {
            ps.setString(1,login);
            try(ResultSet rs = ps.executeQuery()) {
                if (rs.isBeforeFirst() & rs.next()){
                    return User.builder()
                            .id(rs.getLong("id"))
                            .pass(rs.getString("pass"))
                            .login(rs.getString("login"))
                            .userId(rs.getInt("user_id"))
                            .accountId(rs.getInt("account_id"))
                            .user(rs.getString("user"))
                            .account(rs.getString("account"))
                            .roleCode(rs.getInt("role_code"))
                            .build();
                }
                throw new NoSuchUserException("No such user!");
            }
        }
    }

    public boolean logIn(String login, String password) throws SQLException, NoSuchUserException {
        try(Connection connection = connectionPool.getConnection();
            PreparedStatement ps = connection.prepareStatement(LOG_IN)) {
            ps.setString(1,login);
            ps.setString(2, password);
            try(ResultSet rs = ps.executeQuery()) {
                if (rs.isBeforeFirst() & rs.next()){
                    return rs.getInt("count") != 0;
                }
                throw new NoSuchUserException("No such user!");
            }
        }
    }

    public int getUserId(String user) throws NoSuchUserException, SQLException {
        try(Connection connection = connectionPool.getConnection();
            PreparedStatement ps = connection.prepareStatement(SELECT_USER_ID)) {
            ps.setString(1,user);
            try(ResultSet rs = ps.executeQuery()) {
                if (rs.isBeforeFirst() & rs.next()){
                    return rs.getInt("user_id");
                }
                else
                    throw new NoSuchUserException("No user id for" + user);
            }
        }
    }

    public void addOrUpdateUser(User user) throws SQLException {
        try(Connection connection = connectionPool.getConnection();
            PreparedStatement ps = connection.prepareStatement(ADD_UPDATE_USER)
        ) {
            ps.setString(1, user.getLogin());
            ps.setString(2, user.getPass());
            ps.setInt(3, user.getUserId());
            ps.setInt(4, user.getAccountId());
            ps.setString(5, user.getUser());
            ps.setString(6, user.getAccount());
            ps.setInt(7, user.getRoleCode());
            ps.execute();
        }
    }

    public void deleteUser(String user) throws SQLException, NoSuchUserException {
        if (user.equals("root"))
            throw new NoSuchUserException("Ah-ha-ha, no, you can not delete root!!!");
        try(Connection connection = connectionPool.getConnection();
            PreparedStatement ps = connection.prepareStatement(DELETE_USER)
        ) {
            ps.setString(1, user);
            ps.execute();
        }
    }
}

package ru.spbstu.scc.db.slurmacc.entities;

import lombok.*;
import org.json.JSONObject;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class User {
    private long id;
    private String login;
    private String pass;
    private int userId;
    private int accountId;
    private String user;
    private String account;
    private int roleCode;

    public String toJsonString(){
        return toJson().toString();
    }

    public JSONObject toJsonObj(){
        return toJson();
    }

    private JSONObject toJson(){
        JSONObject json = new JSONObject();
        json.put("login", login);
        json.put("pass", pass);
        json.put("userId", userId);
        json.put("accountId", accountId);
        json.put("user", user);
        json.put("account", account);
        json.put("role", roleCode);
        return json;
    }
}

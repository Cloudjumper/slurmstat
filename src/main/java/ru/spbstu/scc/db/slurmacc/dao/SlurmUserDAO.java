package ru.spbstu.scc.db.slurmacc.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Repository;
import ru.spbstu.scc.db.ConnectionPool;
import ru.spbstu.scc.db.slurmacc.entities.SlurmUser;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SlurmUserDAO {
    private static final Logger LOGGER = LogManager.getLogger("FASTD");
    //language=MySQL
    private static final String SELECT_ALL = "SELECT DISTINCT * FROM Slurm_User";
    //language=MySQL
    private static final String SELECT_USER_BY_ID = "SELECT user FROM Slurm_User WHERE user_id=?";
    //language=MySQL
    private static final String ADD_UPDATE_USER = "INSERT INTO Slurm_User (user, user_id) VALUES (?, ?) " +
            "ON DUPLICATE KEY UPDATE user = VALUES(user), user_id = VALUES(user_id)";

    private static ConnectionPool connectionPool;
    private static volatile SlurmUserDAO instance;

    private SlurmUserDAO(){
    }

    public static SlurmUserDAO getInstance(@Qualifier("slurmAccConnectionPool") ConnectionPool connectionPool){
        if (instance != null)
            return instance;
        synchronized (SlurmAccDAO.class){
            if (instance != null)
                return instance;
            instance = new SlurmUserDAO();
            SlurmUserDAO.connectionPool = connectionPool;
            return instance;
        }
    }

    public List<SlurmUser> getNamesWithId() throws SQLException, IOException {
        try(Connection connection = connectionPool.getConnection();
            PreparedStatement ps = connection.prepareStatement(SELECT_ALL);
            ResultSet rs = ps.executeQuery()
        ) {
            List<SlurmUser> all = new ArrayList<>();
            while (rs.next()){
                all.add(SlurmUser.builder()
                        .user(rs.getString("user"))
                        .userId(rs.getInt("user_id"))
                        .build());
            }
            return all;
        }
    }

    public void addOrUpdateUser(SlurmUser user) throws SQLException {
        try(Connection connection = connectionPool.getConnection();
            PreparedStatement ps = connection.prepareStatement(ADD_UPDATE_USER)
        ) {
            ps.setString(1, user.getUser());
            ps.setInt(2, user.getUserId());
            ps.execute();
        } catch (SQLIntegrityConstraintViolationException e){
            LOGGER.error("Try to add duplicated key");
        }
    }

    public String getUserById(int id) throws SQLException {
        try(Connection connection = connectionPool.getConnection();
            PreparedStatement ps = connection.prepareStatement(SELECT_USER_BY_ID)) {
            ps.setInt(1, id);
            try(ResultSet rs = ps.executeQuery()) {
                return rs.next()?rs.getString("user"):String.valueOf(id);
            }
        }
    }
}

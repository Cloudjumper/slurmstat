package ru.spbstu.scc.db.slurmacc.entities;

import lombok.*;

/**
 * Created by cloudjumper on 3/1/17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class SlurmUser {
    private String user;
    private int userId;
}

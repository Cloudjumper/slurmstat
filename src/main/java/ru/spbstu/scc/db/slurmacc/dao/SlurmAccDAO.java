package ru.spbstu.scc.db.slurmacc.dao;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import ru.spbstu.scc.db.ConnectionPool;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SlurmAccDAO {
    //language=MySQL
    private static final String SELECT_ALL = "SELECT DISTINCT * FROM Slurm_Acc";
    //language=MySQL
    private static final String ADD_UPDATE_ACCOUNT = "INSERT INTO Slurm_Acc (account) VALUES (?) " +
            "ON DUPLICATE KEY UPDATE account = VALUES(account)";

    private static ConnectionPool connectionPool;
    private static volatile SlurmAccDAO instance;

    private SlurmAccDAO(){
    }

    public static SlurmAccDAO getInstance(@Qualifier("slurmAccConnectionPool") ConnectionPool connectionPool){
        if (instance != null)
            return instance;
        synchronized (SlurmAccDAO.class){
            if (instance != null)
                return instance;
            instance = new SlurmAccDAO();
            SlurmAccDAO.connectionPool = connectionPool;
            return instance;
        }
    }

    public List<String> getAccounts() throws SQLException, IOException {
        try(Connection connection = connectionPool.getConnection();
            PreparedStatement ps = connection.prepareStatement(SELECT_ALL);
            ResultSet rs = ps.executeQuery()) {
            List<String> all = new ArrayList<>();
            while (rs.next()){
                all.add(rs.getString("account"));
            }
            return all;
        }
    }

    public void addOrUpdateUser(String account) throws SQLException {
        try(Connection connection = connectionPool.getConnection();
            PreparedStatement ps = connection.prepareStatement(ADD_UPDATE_ACCOUNT)
        ) {
            ps.setString(1, account);
            ps.execute();
        }
    }
}

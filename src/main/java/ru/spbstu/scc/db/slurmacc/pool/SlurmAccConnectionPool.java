package ru.spbstu.scc.db.slurmacc.pool;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import ru.spbstu.scc.db.ConnectionPool;
import ru.spbstu.scc.db.slurm.pool.SlurmConnectionPool;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class SlurmAccConnectionPool implements ConnectionPool {
    private static volatile SlurmAccConnectionPool instance;
    private static DataSource datasource = new DataSource();

    private SlurmAccConnectionPool(String propertyPath) {
        Properties properties = new Properties();
        try (FileInputStream fileInputStream = new FileInputStream(propertyPath)) {
            properties.load(fileInputStream);
        } catch (IOException e) {
            System.err.println("Reading properties exception");
        }
        PoolProperties p = new PoolProperties();
        p.setUrl(properties.getProperty("slurmAcc.mysql.url"));//useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&
        p.setDriverClassName(properties.getProperty("slurmAcc.mysql.driver", "com.mysql.cj.jdbc.Driver"));
        p.setUsername(properties.getProperty("slurmAcc.mysql.user"));
        p.setPassword(properties.getProperty("slurmAcc.mysql.pass"));
        p.setJmxEnabled(true);
        p.setTestWhileIdle(false);
        p.setTestOnBorrow(true);
        p.setValidationQuery("SELECT 1");
        p.setTestOnReturn(false);
        p.setValidationInterval(30000);
        p.setTimeBetweenEvictionRunsMillis(30000);
        p.setMaxActive(100);
        p.setInitialSize(10);
        p.setMaxWait(10000);
        p.setRemoveAbandonedTimeout(60);
        p.setMinEvictableIdleTimeMillis(30000);
        p.setMinIdle(10);
        p.setLogAbandoned(true);
        p.setRemoveAbandoned(true);
        p.setJdbcInterceptors(
                "org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;" +
                        "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");
        datasource.setPoolProperties(p);
    }

    public static SlurmAccConnectionPool getInstance() {
        SlurmAccConnectionPool local = instance;
        if (local != null)
            return local;
        synchronized (SlurmConnectionPool.class) {
            local = instance;
            if (local == null)
                instance = local = new SlurmAccConnectionPool(SlurmConnectionPool
                        .class
                        .getResource("/db.properties").getPath());
            return local;
        }
    }

    public Connection getConnection() throws SQLException {
        return datasource.getConnection();
    }
}

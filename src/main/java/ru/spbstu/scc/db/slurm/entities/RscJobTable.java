package ru.spbstu.scc.db.slurm.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * Created by cloudjumper on 1/25/17.
 */
@AllArgsConstructor
@ToString
@Getter
public class RscJobTable {
    private String account;
    private int userId;
    private String partition;
    private Long timeStart;
    private Long timeEnd;
    private Long nodesAlloc;
}

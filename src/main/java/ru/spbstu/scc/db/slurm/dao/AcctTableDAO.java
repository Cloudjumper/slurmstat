package ru.spbstu.scc.db.slurm.dao;


import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Repository;
import ru.spbstu.scc.db.ConnectionPool;
import ru.spbstu.scc.db.slurm.pool.SlurmConnectionPool;
import ru.spbstu.scc.db.slurmacc.dao.SlurmAccDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AcctTableDAO {
    //language=MySQL
    private static final String SELECT_ACCOUNTS = "SELECT DISTINCT name FROM acct_table";

    private static ConnectionPool connectionPool;

    private static volatile AcctTableDAO instance;

    private AcctTableDAO(){
    }

    public static AcctTableDAO getInstance(ConnectionPool connectionPool){
        if (instance != null)
            return instance;
        synchronized (SlurmAccDAO.class){
            if (instance != null)
                return instance;
            instance = new AcctTableDAO();
            AcctTableDAO.connectionPool = connectionPool;
            return instance;
        }
    }

    public List<String> getAccounts() throws SQLException {
        try(Connection connection = connectionPool.getConnection();
            PreparedStatement ps = connection.prepareStatement(SELECT_ACCOUNTS);
            ResultSet rs = ps.executeQuery()
        ) {
            List<String> all = new ArrayList<>();
            while (rs.next()){
                all.add(rs.getString("name"));
            }
            return all;
        }
    }

}

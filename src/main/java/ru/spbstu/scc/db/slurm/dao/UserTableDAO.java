package ru.spbstu.scc.db.slurm.dao;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Repository;
import ru.spbstu.scc.db.ConnectionPool;
import ru.spbstu.scc.db.slurm.pool.SlurmConnectionPool;
import ru.spbstu.scc.db.slurmacc.entities.SlurmUser;
import ru.spbstu.scc.utils.UserId;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserTableDAO {
    //language=MySQL
    private static final String SELECT_NAMES = "SELECT DISTINCT name FROM user_table";

    private static ConnectionPool connectionPool;

    private static volatile UserTableDAO instance;

    private UserTableDAO(){
    }

    public static UserTableDAO getInstance(ConnectionPool connectionPool){
        if (instance != null)
            return instance;
        synchronized (UserTableDAO.class){
            if (instance != null)
                return instance;
            instance = new UserTableDAO();
            UserTableDAO.connectionPool = connectionPool;
            return instance;
        }
    }

    public List<SlurmUser> getNamesWithId() throws SQLException, IOException {
        try(Connection connection = connectionPool.getConnection();
            PreparedStatement ps = connection.prepareStatement(SELECT_NAMES);
            ResultSet rs = ps.executeQuery()) {
            List<SlurmUser> all = new ArrayList<>();
            while (rs.next()){
                all.add(SlurmUser.builder()
                        .user(rs.getString("name"))
                        .userId(UserId.getId(rs.getString("name")))
                        .build());
            }
            return all;
        }
    }
}

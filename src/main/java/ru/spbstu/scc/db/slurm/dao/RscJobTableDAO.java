package ru.spbstu.scc.db.slurm.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Repository;
import ru.spbstu.scc.db.ConnectionPool;
import ru.spbstu.scc.db.slurm.entities.RscJobTable;
import ru.spbstu.scc.db.slurm.pool.SlurmConnectionPool;
import ru.spbstu.scc.db.slurmacc.dao.SlurmAccDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

public class RscJobTableDAO {
    private static final Logger LOGGER = LogManager.getLogger("FASTD");

    private final static String SELECT_FROM_RSC_JOB_TABLE_BY_USER =
            "SELECT *\n" +
            "FROM (SELECT\n" +
            "        account,\n" +
            "        id_user,\n" +
            "        `partition`,\n" +
            "        time_start,\n" +
            "        if(time_end = 0, ?, time_end) AS time_end,\n" +
            "        nodes_alloc\n" +
            "      FROM rsc_job_table\n" +
            "      WHERE id_user = ? AND " +
            "           state > 0 AND " +
            "           rsc_job_table.time_start > 0 AND " +
            "           rsc_job_table.time_end > 0) AS s1\n" +
            "WHERE cast(time_start AS SIGNED) <= ? AND " +
            "       cast(time_end AS SIGNED) >= ? AND" +
            "       cast(time_start AS SIGNED) <= cast(time_end AS SIGNED)" +
            "ORDER BY `partition`, id_user;";

    private final static String SELECT_FROM_RSC_JOB_TABLE_BY_ACCOUNT =
            "SELECT *\n" +
            "FROM (SELECT\n" +
            "        account,\n" +
            "        id_user,\n" +
            "        `partition`,\n" +
            "        time_start,\n" +
            "        if(time_end = 0, ?, time_end) AS time_end,\n" +
            "        nodes_alloc\n" +
            "      FROM rsc_job_table\n" +
            "      WHERE account = ? AND " +
            "           state > 0 AND " +
            "           rsc_job_table.time_start > 0 AND " +
            "           rsc_job_table.time_end > 0) AS s1\n" +
            "WHERE cast(time_start AS SIGNED) <= ? AND " +
            "       cast(time_end AS SIGNED) >= ? AND" +
            "       cast(time_start AS SIGNED) <= cast(time_end AS SIGNED)"+
            "ORDER BY `partition`, id_user;";

    // language=MySQL
    private final static String SELECT_FROM_RSC_JOB_TABLE_BY_PARTITION =
            "SELECT *\n" +
            "FROM (SELECT\n" +
            "        account,\n" +
            "        id_user,\n" +
            "        `partition`,\n" +
            "        time_start,\n" +
            "        if(time_end = 0, ?, time_end) AS time_end,\n" +
            "        nodes_alloc\n" +
            "      FROM rsc_job_table\n" +
            "      WHERE `partition` = ? AND " +
            "           state > 0 AND " +
            "           rsc_job_table.time_start > 0 AND" +
            "           rsc_job_table.time_end > 0) AS s1\n" +
            "WHERE cast(time_start AS SIGNED) <= ? AND\n" +
            "      cast(time_end AS SIGNED) >= ? AND\n" +
            "      cast(time_start AS SIGNED) <= cast(time_end AS SIGNED)\n" +
            "ORDER BY `partition`, account, id_user;";

    private static ConnectionPool connectionPool;

    private static volatile RscJobTableDAO instance;

    private RscJobTableDAO(){
    }

    public static RscJobTableDAO getInstance(ConnectionPool connectionPool){
        if (instance != null)
            return instance;
        synchronized (SlurmAccDAO.class){
            if (instance != null)
                return instance;
            instance = new RscJobTableDAO();
            RscJobTableDAO.connectionPool = connectionPool;
            return instance;
        }
    }

    @SuppressWarnings("Duplicates")
    public List<RscJobTable> getByAccount(String account, long start, long end) throws SQLException {
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement ps = connection.prepareStatement(SELECT_FROM_RSC_JOB_TABLE_BY_ACCOUNT)) {
            ps.setLong(1, LocalDateTime.now().atZone(ZoneId.systemDefault()).toEpochSecond());
            ps.setString(2, account);
            ps.setLong(3,end);
            ps.setLong(4,start);
            try(ResultSet rs = ps.executeQuery()) {
                List<RscJobTable> res = new ArrayList<>();
                while (rs.next()) {
                    res.add(new RscJobTable(rs.getString("account"),
                            rs.getInt("id_user"),
                            rs.getString("partition"),
                            rs.getLong("time_start"),
                            rs.getLong("time_end"),
                            rs.getLong("nodes_alloc")));
                }
                return res;
            }
        }
    }

    @SuppressWarnings("Duplicates")
    public List<RscJobTable> getByUserId(int userId, long start, long end) throws SQLException {
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement ps = connection.prepareStatement(SELECT_FROM_RSC_JOB_TABLE_BY_USER)) {
            ps.setLong(1, LocalDateTime.now().atZone(ZoneId.systemDefault()).toEpochSecond());
            ps.setInt(2, userId);
            ps.setLong(3,end);
            ps.setLong(4,start);
            try(ResultSet rs = ps.executeQuery()) {
                List<RscJobTable> res = new ArrayList<>();
                while (rs.next()) {
                    res.add(new RscJobTable(rs.getString("account"),
                            rs.getInt("id_user"),
                            rs.getString("partition"),
                            rs.getLong("time_start"),
                            rs.getLong("time_end"),
                            rs.getLong("nodes_alloc"))
                    );
                }
                return res;
            }
        }
    }

    @SuppressWarnings("Duplicates")
    public List<RscJobTable> getByPartition(String partition, long start, long end) throws SQLException {
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement ps = connection.prepareStatement(SELECT_FROM_RSC_JOB_TABLE_BY_PARTITION)) {
            ps.setLong(1, LocalDateTime.now().atZone(ZoneId.systemDefault()).toEpochSecond());
            ps.setString(2, partition);
            ps.setLong(3,end);
            ps.setLong(4,start);
            try(ResultSet rs = ps.executeQuery()) {
                List<RscJobTable> res = new ArrayList<>();
                while (rs.next()) {
                    res.add(new RscJobTable(rs.getString("account"),
                            rs.getInt("id_user"),
                            rs.getString("partition"),
                            rs.getLong("time_start"),
                            rs.getLong("time_end"),
                            rs.getLong("nodes_alloc"))
                    );
                }
                return res;
            }
        }
    }
}

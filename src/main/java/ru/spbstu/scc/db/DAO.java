package ru.spbstu.scc.db;

import java.util.Collection;

/**
 * @deprecated CRUD not best practices, coz some time we need special requests
 * @param <T1>
 * @param <T2>
 * @param <R1>
 */
@Deprecated
public interface DAO<T1, T2, R1 extends Collection<T1>> {
    void insert(T1 item);
    T1 select(T2 param);
    R1 selectAll(T2 param);
    void update(T1 item);
    void delete(T2 param);
}

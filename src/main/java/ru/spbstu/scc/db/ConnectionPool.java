package ru.spbstu.scc.db;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by cloudjumper on 4/18/17.
 */
public interface ConnectionPool {
    Connection getConnection() throws SQLException;
}

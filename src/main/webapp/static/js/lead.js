/**
 * Created by cloudjumper on 2/20/17.
 */
$("#account-statistic-submit").click(function () {
    $.ajax({
        url: "/stat/account",
        type: 'GET',
        data: {
            'sTime': $("#account-statistic-stime").val(),
            'eTime': $("#account-statistic-etime").val()
        },
        success: function(response){
            console.log(response);
            $("#account-statistic-error").hide("fade", 200);
            // const array = $.parseJSON(response).client;
            $("#account-statistic-results").children("table").empty();
            $("#account-statistic-results").children("table").append(
                "<tr>" +
                "   <th>" + dict[lang]["partition"] + "</th>" +
                "   <th>" + dict[lang]["user"] + "</th>" +
                "   <th>" + dict[lang]["account"] + "</th>" +
                "   <th>" + dict[lang]["workTime"] + "</th>" +
                "</tr>");
            $.each($.parseJSON(response).client, function (index, item) {
                $("#account-statistic-results").children("table")
                    .append("<tr><td>" +
                        item.partition +
                        "</td><td>" +
                        item.userId +
                        "</td><td>" +
                        item.account +
                        "</td><td>" +
                        item.workTime +
                        "</td></tr>");
            });
            $("#account-statistic-results").children("table").append(
                "<tr>" +
                "   <th>" + dict[lang]["partition"] + "</th>" +
                "   <th>---</th>" +
                "   <th>---</th>" +
                "   <th>" + dict[lang]["workTimeTotal"] + "</th>" +
                "</tr>");
            $.each($.parseJSON(response).total, function (key, val) {
                $("#account-statistic-results").children("table").append(
                    "<tr>" +
                    "   <td>" + key + "</td>" +
                    "   <td></td>" +
                    "   <td></td>" +
                    "   <td>" + val + "</td>" +
                    "</tr>");
            });
        },
        error: function(response, status, error){
            $("#account-statistic-error").show("fade", 200);
            $("#account-statistic-error").text(response.responseText);
        }
    });
});
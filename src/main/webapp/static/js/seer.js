var jsonResponse;
var sortColumn; // count from 1, negative for desc

$("#button-user-statistic").click(function () {
    $("#account-statistic").hide();
    $("#partition-statistic").hide();
    $("#user-statistic").show('fade', 200);
});

$("#button-account-statistic").click(function () {
    $("#user-statistic").hide();
    $("#partition-statistic").hide();
    $("#account-statistic").show('fade', 200);
});

$("#button-partition-statistic").click(function () {
    $("#user-statistic").hide();
    $("#account-statistic").hide();
    $("#partition-statistic").show('fade', 200);
});

$("#user-statistic-submit").click(function () {
    $.ajax({
        url: "/stat/user",
        type: 'GET',
        data: {
            'user': $("#user-statistic-user").val().match(/(?!\()\d+(?=\))/)[0],
            'sTime': $("#user-statistic-stime").val(),
            'eTime': $("#user-statistic-etime").val()
        },
        success: function(response){
            $("#user-statistic-error").hide("fade", 200);
            var array = $.parseJSON(response).client;
            $("#user-statistic-results").children("table").empty();
            $("#user-statistic-results").children("table").append(
                "<tr>" +
                "   <th>" + dict[lang]["partition"] + "</th>" +
                "   <th>" + dict[lang]["user"] + "</th>" +
                "   <th>" + dict[lang]["account"] + "</th>" +
                "   <th>" + dict[lang]["workTime"] + "</th>" +
                "</tr>"
            );
            array.forEach(function (item, i, array){
                $("#user-statistic-results").children("table")
                    .append("<tr><td>" +
                        item.partition +
                        "</td><td>" +
                        item.userId +
                        "</td><td>" +
                        item.account +
                        "</td><td>" +
                        item.workTime +
                        "</td></tr>");
                // alert(item.partition + ":" + item.workTime)
            });
        },
        error: function(response, status, error){
            $("#user-statistic-error").show("fade", 200);
            $("#user-statistic-error").text(response.responseText);
        }
    });
});

$("#account-statistic-submit").click(function () {
    $.ajax({
        url: "/stat/account",
        type: 'GET',
        data: {
            'account': $("#account-statistic-account").val(),
            'sTime': $("#account-statistic-stime").val(),
            'eTime': $("#account-statistic-etime").val()
        },
        success: function(response){
            $("#account-statistic-error").hide("fade", 200);
            $("#account-statistic-results").children("table").empty();
            $("#account-statistic-results").children("table").append(
                "<tr>" +
                "   <th>" + dict[lang]["partition"] + "</th>" +
                "   <th>" + dict[lang]["user"] + "</th>" +
                "   <th>" + dict[lang]["account"] + "</th>" +
                "   <th>" + dict[lang]["workTime"] + "</th>" +
                "</tr>"
            );
            $.each($.parseJSON(response).client, function (index, item) {
                $("#account-statistic-results").children("table")
                    .append("<tr><td>" +
                        item.partition +
                        "</td><td>" +
                        item.userId +
                        "</td><td>" +
                        item.account +
                        "</td><td>" +
                        item.workTime +
                        "</td></tr>");
            });
            $("#account-statistic-results").children("table").append(
                "<tr>" +
                "   <th>" + dict[lang]["partition"] + "</th>" +
                "   <th>---</th>" +
                "   <th>---</th>" +
                "   <th>" + dict[lang]["workTimeTotal"] + "</th>" +
                "</tr>"
            );
            $.each($.parseJSON(response).total, function (key, val) {
                $("#account-statistic-results").children("table")
                    .append("<tr><td>" + key + "</td><td></td><td></td><td>" + val + "</td></tr>");
            });
        },
        error: function(response, status, error){
            $("#account-statistic-error").show("fade", 200);
            $("#account-statistic-error").text(response.responseText);
        }
    });
});

function jsonSort(arr, prop, asc) {
    return arr.sort(function(a, b) {
        if (asc) {
            return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
        } else {
            return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
        }
    });
}

function partitionStatRedraw(json) {
    $("#partition-statistic-error").hide("fade", 200);
    $("#partition-statistic-results").children("table:nth-child(1)").empty();
    $("#partition-statistic-results").children("table:nth-child(2)").empty();
    $("#partition-statistic-results").children("table:nth-child(1)").append(
        "<tr>" +
        "   <th>" + dict[lang]["account"] + "&#8645;</th>" +
        "   <th>" + dict[lang]["workTime"] + "&#8645;</th>" +
        "   <th>" + dict[lang]["partWorkTime"] + "&#8645;</th>" +
        "</tr>"
    );
    $.each(json["client"], function (index, item) {
        $("#partition-statistic-results").children("table:nth-child(1)")
            .append(
                "<tr>" +
                "   <td>" + item["account"] + "</td>" +
                "   <td>" + item["workTime"]+ "</td>" +
                "   <td>" + item["partWorkTime"] + "</td>" +
                "</tr>"
            );
    });
    $("#partition-statistic-results").children("table:nth-child(2)").append(
        "<tr>" +
        "   <th>" + dict[lang]["maxWorkTime"] + "</th>" +
        "   <th>" + dict[lang]["sumWorkTime"] + "</th>" +
        "   <th>" + dict[lang]["load"] + "</th>" +
        "</tr>");

    $("#partition-statistic-results").children("table:nth-child(2)").append(
        "<tr>" +
        "   <td>" + json["total"]["maxWorkTime"] + "</td>" +
        "   <td>" + json["total"]["sumWorkTime"] + "</td>" +
        "   <td>" + json["total"]["load"] + "</td>" +
        "</tr>");
}

$("#partition-statistic-submit").click(function () {
    $.ajax({
        url: "/api/stat/partition",
        type: 'GET',
        data: {
            'partition': $("#partition-statistic-partition").val(),
            'sTime': $("#partition-statistic-stime").val(),
            'eTime': $("#partition-statistic-etime").val()
        },
        dataType: "json",
        success: function (response) {
            jsonResponse = response;
            jsonResponse['client'] = jsonSort(jsonResponse['client'], "workTime", true);
            sortColumn = -2; // next sort must be negative
            partitionStatRedraw(jsonResponse);
        },
        error: function (response, status, error) {
            $("#account-statistic-error").show("fade", 200);
            $("#account-statistic-error").text(response.responseText);
        }
    });
});

$("#partition-statistic-results").on("click", "table:nth-child(1) > tr > th:nth-child(1)", function () {
    if(Math.abs(sortColumn) != 1)
        sortColumn = 1;
    jsonResponse['client'] = jsonSort(jsonResponse['client'], "account", 1 == sortColumn);
    sortColumn*=-1;
    partitionStatRedraw(jsonResponse);
});

$("#partition-statistic-results").on("click", "table:nth-child(1) > tr > th:nth-child(2)", function () {
    if(Math.abs(sortColumn) != 2)
        sortColumn = 2;
    jsonResponse['client'] = jsonSort(jsonResponse['client'], "workTime", 2 == sortColumn);
    sortColumn*=-1;
    partitionStatRedraw(jsonResponse);
});

$("#partition-statistic-results").on("click", "table:nth-child(1) > tr > th:nth-child(3)", function () {
    if(Math.abs(sortColumn) != 3)
        sortColumn = 3;
    jsonResponse['client'] = jsonSort(jsonResponse['client'], "partWorkTime", 3 == sortColumn);
    sortColumn*=-1;
    partitionStatRedraw(jsonResponse);
});
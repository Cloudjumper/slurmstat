/**
 * Created by cloudjumper on 2/20/17.
 */
$("#user-statistic-submit").click(function () {
    $.ajax({
        url: "/stat/user",
        type: 'GET',
        data: {
            'sTime': $("#user-statistic-stime").val(),
            'eTime': $("#user-statistic-etime").val()
        },
        success: function(response){
            console.log(response);
            $("#user-statistic-error").hide("fade", 200);
            var array = $.parseJSON(response).client;
            $("#user-statistic-results").children("table").empty();
            $("#user-statistic-results").children("table").append(
                "<tr>" +
                "   <th>" + dict[lang]["partition"] + "</th>" +
                "   <th>" + dict[lang]["user"] + "</th>" +
                "   <th>" + dict[lang]["account"] + "</th>" +
                "   <th>" + dict[lang]["workTime"] + "</th>" +
                "</tr>");
            array.forEach(function (item, i, array){
                $("#user-statistic-results").children("table")
                    .append("<tr><td>" +
                        item.partition +
                        "</td><td>" +
                        item.userId +
                        "</td><td>" +
                        item.account +
                        "</td><td>" +
                        item.workTime +
                        "</td></tr>");
                // alert(item.partition + ":" + item.workTime)
            });
        },
        error: function(response, status, error){
            $("#user-statistic-error").show("fade", 200);
            $("#user-statistic-error").text(response.responseText);
        }
    });
});
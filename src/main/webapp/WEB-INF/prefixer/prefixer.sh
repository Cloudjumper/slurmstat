#!/usr/bin/env bash
# https://github.com/less/less-plugin-autoprefix
# https://github.com/postcss/autoprefixer
# sudo ln -s /usr/bin/nodejs /usr/bin/node
postcss $1/*.css --use autoprefixer --no-map -d $2
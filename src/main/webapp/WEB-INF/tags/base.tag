<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@tag description="base" pageEncoding="UTF-8" %>
<%@attribute name="extraCss" fragment="true" %>
<%@attribute name="extraJs" fragment="true" %>
<%@attribute name="title" fragment="true" %>
<%@attribute name="headerMenu" fragment="true" %>
<%@attribute name="content" fragment="true" %>
<c:set var="lang" scope="session" value="${not empty sessionScope.lang ? sessionScope.lang : 'ru'}"/>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="localization" var="bundle"/>
<html>
<head>
    <title><jsp:invoke fragment="title"/></title>
    <link rel="shortcut icon" href="<c:url context="/" value="/static/img/favicon.ico"/>" />
    <link href="<c:url context="/" value="/static/css/base.css"/>" rel="stylesheet">
    <link href="<c:url context="/" value="/static/css/loader.css"/>" rel="stylesheet">
    <jsp:invoke fragment="extraCss"/>
</head>
<body>
<input id="_csrf" type="hidden" name="${_csrf.parameterName}" 	value="${_csrf.token}" />
<div id="loader">
    <div>
        <div class="windows8">
            <div class="wBall" id="wBall_1">
                <div class="wInnerBall"></div>
            </div>
            <div class="wBall" id="wBall_2">
                <div class="wInnerBall"></div>
            </div>
            <div class="wBall" id="wBall_3">
                <div class="wInnerBall"></div>
            </div>
            <div class="wBall" id="wBall_4">
                <div class="wInnerBall"></div>
            </div>
            <div class="wBall" id="wBall_5">
                <div class="wInnerBall"></div>
            </div>
        </div>
    </div>
</div>
<div id="header">
    <div class="left-space">

    </div>
    <div id="header-data">
        <div id="title">
            <div>
                <div id="menu" style="height: 50px; width: 50px">
                    <div id="line1" class="line"></div>
                    <div id="line2" class="line"></div>
                    <div id="line3" class="line"></div>
                    <div id="line4" class="line"></div>
                    <div id="line5" class="line"></div>
                    <div id="line6" class="line"></div>
                    <div id="line7" class="line"></div>
                    <jsp:invoke fragment="headerMenu"/>
                </div>
                <%--<a href="<c:url context="/" value="/"/>">--%>
                    <%--<img style="height: 50px; width: 50px" alt="Logo" src="<c:url context="/" value="/static/img/P.png"/>">--%>
                <%--</a>--%>
                <div><fmt:message bundle="${bundle}" key="web.base.title"/></div>
            </div>
        </div>
        <div id="head-space"></div>
        <div id="login">
            <c:choose>
                <c:when test="${not empty sessionScope.sUser}">
                    <div id="login-reserve">
                        <input type="button" class="button-header button-login-menu" value=${sUser.login}>
                    </div>
                    <div class="login-form">
                        <div>
                            <fmt:message var="logOut" bundle="${bundle}" key="web.base.logOut"/>
                            <div>
                                <input class="button-login" id="logout-btn-h" type="button" value="${logOut}">
                            </div>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div id="login-reserve">
                        <fmt:message var="logIn" bundle="${bundle}" key="web.base.logIn"/>
                        <input type="button" class="button-header button-login-menu" value="${logIn}">
                    </div>
                    <div class="login-form">
                        <div>
                            <fmt:message var="login" bundle="${bundle}" key="web.base.login"/>
                            <div>
                                <input class="text-login key-login" autocomplete="off" id="login-login-h" type="text"
                                       placeholder="${logIn}">
                            </div>
                            <fmt:message var="pass" bundle="${bundle}" key="web.base.pass"/>
                            <div>
                                <input class="text-login key-login" id="login-pass-h" type="password"
                                       placeholder="${pass}">
                            </div>
                            <div>
                                <input class="button-login key-login" id="login-btn-h" type="button"
                                       value="${logIn}">
                            </div>
                            <div id="login-err-h">
                                Error text!
                            </div>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
    <div class="right-space">

    </div>
</div>
<div id="main">
    <div class="left-space">

    </div>
    <div id="main-data">
        <jsp:invoke fragment="content"/>
    </div>
    <div class="right-space">

    </div>
</div>
<div id="footer">
    <div class="left-space"></div>
    <div id="footer-data">
        <div style="float: left">
            <div id="lang">
                <span>
                    <c:choose>
                        <c:when test="${sessionScope.lang == 'ru'}">
                            Русский
                        </c:when>
                        <c:when test="${sessionScope.lang == 'en'}">
                            English
                        </c:when>
                        <c:otherwise>
                            Русский
                        </c:otherwise>
                    </c:choose>
                </span>
                <div id="lang-menu">
                    <div class="lang-btn" l="ru">
                        <div>
                            Русский
                        </div>
                    </div>
                    <div class="lang-btn" l="en">
                        <div>
                            English
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="float: right">
            <span>
                <c:if test="${not empty sessionScope.get('lastCon')}">
                    <fmt:message bundle="${bundle}" key="web.base.footer.connection"/> ${sessionScope.get("lastCon")}&nbsp;
                </c:if>
            </span>
            <span style="color: white">
                <c:choose>
                    <c:when test="${sessionScope.get('sUser').roleCode == 0}">
                        <fmt:message bundle="${bundle}" key="web.base.footer.access"/>:
                        <fmt:message bundle="${bundle}" key="web.base.footer.access.root"/>
                    </c:when>
                    <c:when test="${sessionScope.get('sUser').roleCode == 1}">
                        <fmt:message bundle="${bundle}" key="web.base.footer.access"/>:
                        <fmt:message bundle="${bundle}" key="web.base.footer.access.seer"/>
                    </c:when>
                    <c:when test="${sessionScope.get('sUser').roleCode == 2}">
                        <fmt:message bundle="${bundle}" key="web.base.footer.user"/>:
                        ${sessionScope.get('sUser').user}(${sessionScope.get('sUser').userId})
                        <fmt:message bundle="${bundle}" key="web.base.footer.group"/>:
                        ${sessionScope.get('sUser').account}(${sessionScope.get('sUser').accountId})
                        <fmt:message bundle="${bundle}" key="web.base.footer.access"/>:
                        <fmt:message bundle="${bundle}" key="web.base.footer.access.lead"/>
                    </c:when>
                    <c:when test="${sessionScope.get('sUser').roleCode == 3}">
                        <fmt:message bundle="${bundle}" key="web.base.footer.user"/>:
                        ${sessionScope.get('sUser').user}(${sessionScope.get('sUser').userId})
                        <fmt:message bundle="${bundle}" key="web.base.footer.group"/>:
                        ${sessionScope.get('sUser').account}(${sessionScope.get('sUser').accountId})
                        <fmt:message bundle="${bundle}" key="web.base.footer.access"/>:
                        <fmt:message bundle="${bundle}" key="web.base.footer.access.user"/>
                    </c:when>
                    <c:otherwise>
                        <fmt:message bundle="${bundle}" key="web.base.footer.access"/>:
                        <fmt:message bundle="${bundle}" key="web.base.footer.access.guest"/>
                    </c:otherwise>
                </c:choose>
            </span>
        </div>
    </div>
    <div class="right-space"></div>
</div>
</body>
<script type="application/javascript" src="<c:url context="/" value="/static/js/jquery-3.1.1.min.js"/>"></script>
<script type="application/javascript" src="<c:url context="/" value="/static/js/jquery-ui.min.js"/>"></script>
<script type="application/javascript" src="<c:url context="/" value="/static/js/sha1.min.js"/>"></script>
<script type="application/javascript" src="<c:url context="/" value="/static/js/base.js"/>"></script>
<jsp:invoke fragment="extraJs"/>
</html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link type="text/css" href="<c:url context="/" value="/static/css/error.css"/>" rel="stylesheet">
    <title>4**</title>
</head>
<body>
<div id="header">
    <div class="left-space">

    </div>
    <div id="header-data">
        <div id="title">
            <div>
                4** - Client error
            </div>
        </div>
    </div>
    <div class="right-space">

    </div>
</div>
<div id="main">
    <div class="left-space">

    </div>
    <div id="main-data">
        <div>
            ${msg}
        </div>
        <div>
            ${user}
        </div>
    </div>
    <div class="right-space">

    </div>
</div>
</body>
</html>
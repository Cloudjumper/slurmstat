<%--
  Created by IntelliJ IDEA.
  User: cloudjumper
  Date: 2/17/17
  Time: 10:04 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="lang" scope="session" value="${not empty sessionScope.lang ? sessionScope.lang : 'ru'}"/>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="localization" var="bundle"/>
<t:base>
    <jsp:attribute name="extraJs">
        <script type="application/javascript" src="/static/js/lead.js"></script>
        <script type="application/javascript" src="/static/js/datetimepicker.js"></script>
        <script type="application/javascript" src="/static/js/datetimepicker.init.js"></script>
    </jsp:attribute>
    <jsp:attribute name="extraCss">
        <link href="/static/css/lead.css" rel="stylesheet"/>
        <link rel="stylesheet" type="text/css" href="/static/css/datetimepicker.css" >
    </jsp:attribute>
    <jsp:attribute name="content">
        <div id="account-statistic">
            <div id="account-statistic-title">
                <fmt:message bundle="${bundle}" key="web.common.accountStat.title"/>
            </div>
            <div>
                <div id="account-statistic-actions">
                    <fmt:message var="startTime" bundle="${bundle}" key="web.common.startTime"/>
                    <div>
                        <input id="account-statistic-stime" class="datetimepicker-stime text-all" type="text" placeholder="${startTime}">
                    </div>
                    <fmt:message var="endTime" bundle="${bundle}" key="web.common.endTime"/>
                    <div>
                        <input id="account-statistic-etime" class="datetimepicker-etime text-all" type="text" placeholder="${endTime}">
                    </div>
                    <fmt:message var="stat" bundle="${bundle}" key="web.common.getStat"/>
                    <div>
                        <input id="account-statistic-submit" class="button-all" type="button" value="${stat}">
                    </div>
                </div>
                <div id="account-statistic-results">
                    <table>
                        <tr>
                            <th><fmt:message bundle="${bundle}" key="web.common.partition"/></th>
                            <th><fmt:message bundle="${bundle}" key="web.common.user"/></th>
                            <th><fmt:message bundle="${bundle}" key="web.common.account"/></th>
                            <th><fmt:message bundle="${bundle}" key="web.common.workTime"/></th>
                        </tr>
                    </table>
                </div>
            </div>
            <div id="account-statistic-error">

            </div>
        </div>
    </jsp:attribute>
</t:base>

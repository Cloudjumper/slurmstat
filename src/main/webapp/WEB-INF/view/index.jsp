<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="lang" scope="session" value="${not empty sessionScope.lang ? sessionScope.lang : 'ru'}"/>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="localization" var="bundle"/>
<t:base>
    <jsp:attribute name="title">Welcome!</jsp:attribute>
    <jsp:attribute name="extraJs">
    </jsp:attribute>
    <jsp:attribute name="extraCss">
        <link href="<c:url context="/" value="/static/css/index.css"/>" rel="stylesheet" type="text/css"/>
    </jsp:attribute>
    <jsp:attribute name="content">
        <div>
            Please, log in!
        </div>
        <div>
            And also there will be some instructions for users... if you agree. Or any other information.
        </div>
    </jsp:attribute>
</t:base>


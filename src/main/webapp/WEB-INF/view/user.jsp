<%--
  Created by IntelliJ IDEA.
  User: cloudjumper
  Date: 2/17/17
  Time: 10:04 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="lang" scope="session" value="${not empty sessionScope.lang ? sessionScope.lang : 'ru'}"/>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="localization" var="bundle"/>
<t:base>
    <jsp:attribute name="title">
        User page
    </jsp:attribute>
    <jsp:attribute name="extraJs">
        <script type="application/javascript" src="<c:url context="/" value="/static/js/user.js"/>"></script>
        <script type="application/javascript" src="<c:url context="/" value="/static/js/datetimepicker.js"/>"></script>
        <script type="application/javascript" src="<c:url context="/" value="/static/js/datetimepicker.init.js"/>"></script>
    </jsp:attribute>
    <jsp:attribute name="extraCss">
        <link href="<c:url context="/" value="/static/css/user.css"/>" rel="stylesheet"/>
        <link href="<c:url context="/" value="/static/css/datetimepicker.css"/>" rel="stylesheet"/>
    </jsp:attribute>
    <jsp:attribute name="content">
        <div id="user-statistic">
            <div id="user-statistic-title">
                <fmt:message bundle="${bundle}" key="web.common.userStat.title"/>
            </div>
            <div>
                <div id="user-statistic-actions">
                    <fmt:message var="startTime" bundle="${bundle}" key="web.common.startTime"/>
                    <div>
                        <input id="user-statistic-stime" class="datetimepicker-stime text-all" type="text" placeholder="${startTime}">
                    </div>
                    <fmt:message var="endTime" bundle="${bundle}" key="web.common.endTime"/>
                    <div>
                        <input id="user-statistic-etime" class="datetimepicker-etime text-all" type="text" placeholder="${endTime}">
                    </div>
                    <fmt:message var="stat" bundle="${bundle}" key="web.common.getStat"/>
                    <div>
                        <input id="user-statistic-submit" class="button-all" type="button" value="${stat}">
                    </div>
                </div>
                <div id="user-statistic-results">
                    <table>
                        <tr>
                            <th><fmt:message bundle="${bundle}" key="web.common.partition"/></th>
                            <th><fmt:message bundle="${bundle}" key="web.common.user"/></th>
                            <th><fmt:message bundle="${bundle}" key="web.common.account"/></th>
                            <th><fmt:message bundle="${bundle}" key="web.common.workTime"/></th>
                        </tr>
                    </table>
                </div>
            </div>
            <div id="user-statistic-error">

            </div>
        </div>
    </jsp:attribute>
</t:base>

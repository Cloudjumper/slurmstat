<%--
  Created by IntelliJ IDEA.
  User: cloudjumper
  Date: 2/17/17
  Time: 10:04 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="lang" scope="session" value="${not empty sessionScope.lang ? sessionScope.lang : 'ru'}"/>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="localization" var="bundle"/>
<t:base>
    <jsp:attribute name="title">
        Seer
    </jsp:attribute>
    <jsp:attribute name="headerMenu">
        <div id="title-menu">
            <div class="header-menu">
                <div>
                    <fmt:message var="userInfo" bundle="${bundle}" key="web.menu.userInfo"/>
                    <input id="button-user-statistic" class="button-menu" type="button" value="${userInfo}">
                </div>
                <div>
                    <fmt:message var="accountInfo" bundle="${bundle}" key="web.menu.accountInfo"/>
                    <input id="button-account-statistic" class="button-menu" type="button" value="${accountInfo}">
                </div>
                <div>
                    <fmt:message var="partitionInfo" bundle="${bundle}" key="web.menu.partitionInfo"/>
                    <input id="button-partition-statistic" class="button-menu" type="button" value="${partitionInfo}">
                </div>
            </div>
        </div>
    </jsp:attribute>
    <jsp:attribute name="extraJs">
        <script type="application/javascript" src="<c:url context="/" value="/static/js/seer.js"/>"></script>
        <script type="application/javascript" src="<c:url context="/" value="/static/js/datetimepicker.js"/>"></script>
        <script type="application/javascript" src="<c:url context="/" value="/static/js/datetimepicker.init.js"/>"></script>
    </jsp:attribute>
    <jsp:attribute name="extraCss">
        <link rel="stylesheet" type="text/css" href="<c:url context="/" value="/static/css/seer.css"/>"/>
        <link rel="stylesheet" type="text/css" href="<c:url context="/" value="/static/css/datetimepicker.css"/>">
    </jsp:attribute>
    <jsp:attribute name="content">
        <div id="user-statistic" class="show1 show3 content-block">
            <div class="title">
                <fmt:message bundle="${bundle}" key="web.common.userStat.title"/>
            </div>
            <div>
                <div id="user-statistic-actions">
                    <div>
                        <select style="width: 100%" autocomplete="false" id="user-statistic-user" class="select-all select-admin" title="Users">
                            <c:forEach items="${slurmUsers}" var="item">
                                <option>${item.user} (${item.userId})</option>
                            </c:forEach>
                        </select>
                    </div>
                    <fmt:message var="startTime" bundle="${bundle}" key="web.common.startTime"/>
                    <div>
                        <input autocomplete="false" id="user-statistic-stime" class="datetimepicker-stime text-all text-admin" type="text" placeholder="${startTime}">
                    </div>
                    <fmt:message var="endTime" bundle="${bundle}" key="web.common.endTime"/>
                    <div>
                        <input autocomplete="false" id="user-statistic-etime" class="datetimepicker-etime text-all text-admin" type="text" placeholder="${endTime}">
                    </div>
                    <fmt:message var="stat" bundle="${bundle}" key="web.common.getStat"/>
                    <div>
                        <input id="user-statistic-submit" class="button-all button-admin" type="button" value="${stat}">
                    </div>
                </div>
                <div id="user-statistic-results">
                    <table>
                        <tr>
                            <th><fmt:message bundle="${bundle}" key="web.common.partition"/></th>
                            <th><fmt:message bundle="${bundle}" key="web.common.user"/></th>
                            <th><fmt:message bundle="${bundle}" key="web.common.account"/></th>
                            <th><fmt:message bundle="${bundle}" key="web.common.workTime"/></th>
                        </tr>
                    </table>
                </div>
            </div>
            <div id="user-statistic-error">

            </div>
        </div>
        <div id="account-statistic" class="show1 show2 content-block">
            <div class="title">
                <fmt:message bundle="${bundle}" key="web.common.accountStat.title"/>
            </div>
            <div>
                <div id="account-statistic-actions">
                    <div>
                        <select style="width: 100%" autocomplete="false" id="account-statistic-account" class="select-all select-admin" title="Account">
                            <c:forEach items="${slurmAccounts}" var="item">
                                <option>${item}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <fmt:message var="startTime" bundle="${bundle}" key="web.common.startTime"/>
                    <div>
                        <input autocomplete="false" id="account-statistic-stime"
                               class="datetimepicker-stime text-all text-admin" type="text" placeholder="${startTime}">
                    </div>
                    <fmt:message var="endTime" bundle="${bundle}" key="web.common.endTime"/>
                    <div>
                        <input autocomplete="false" id="account-statistic-etime"
                               class="datetimepicker-etime text-all text-admin" type="text" placeholder="${endTime}">
                    </div>
                    <fmt:message var="stat" bundle="${bundle}" key="web.common.getStat"/>
                    <div>
                        <input id="account-statistic-submit" class="button-all button-admin" type="button" value="${stat}">
                    </div>
                </div>
                <div id="account-statistic-results">
                    <table>
                        <tr>
                            <th><fmt:message bundle="${bundle}" key="web.common.partition"/></th>
                            <th><fmt:message bundle="${bundle}" key="web.common.user"/></th>
                            <th><fmt:message bundle="${bundle}" key="web.common.account"/></th>
                            <th><fmt:message bundle="${bundle}" key="web.common.workTime"/></th>
                        </tr>
                    </table>
                </div>
            </div>
            <div id="account-statistic-error"></div>
        </div>
        <div id="partition-statistic" class="content-block">
            <div class="title">
                <fmt:message bundle="${bundle}" key="web.common.partitionStat.title"/>
            </div>
            <div>
                <div id="partition-statistic-actions">
                    <div>
                        <select style="width: 100%" autocomplete="false" id="partition-statistic-partition" class="select-all select-admin" title="Account">
                            <c:forEach items="${slurmPartitions}" var="item">
                                <option>${item}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <fmt:message var="startTime" bundle="${bundle}" key="web.common.startTime"/>
                    <div>
                        <input autocomplete="false" id="partition-statistic-stime" class="datetimepicker-stime text-all text-admin" type="text" placeholder="${startTime}">
                    </div>
                    <fmt:message var="endTime" bundle="${bundle}" key="web.common.endTime"/>
                    <div>
                        <input autocomplete="false" id="partition-statistic-etime" class="datetimepicker-etime text-all text-admin" type="text" placeholder="${endTime}">
                    </div>
                    <fmt:message var="stat" bundle="${bundle}" key="web.common.getStat"/>
                    <div>
                        <input id="partition-statistic-submit" class="button-all button-admin" type="button" value="${stat}">
                    </div>
                </div>
                <div id="partition-statistic-results">
                    <table>
                        <tr>
                            <th><fmt:message bundle="${bundle}" key="web.common.account"/>&#8645;</th>
                            <th><fmt:message bundle="${bundle}" key="web.common.workTime"/>&#8645;</th>
                            <th><fmt:message bundle="${bundle}" key="web.common.partWorkTime"/>&#8645;</th>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <th><fmt:message bundle="${bundle}" key="web.common.maxWorkTime"/></th>
                            <th><fmt:message bundle="${bundle}" key="web.common.sumWorkTime"/></th>
                            <th><fmt:message bundle="${bundle}" key="web.common.load"/></th>
                        </tr>
                    </table>
                </div>
            </div>
            <div id="partition-statistic-error"></div>
        </div>
    </jsp:attribute>
</t:base>

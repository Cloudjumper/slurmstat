package ru.spbstu.scc.db.slurmacc.dao;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.spbstu.scc.configs.SpringConfig;
import ru.spbstu.scc.db.slurmacc.entities.User;

/**
 * Created by cloudjumper on 4/18/17.
 */
@Ignore
public class UserDAOTest {
//    private static final BeanFactory BEAN_FACTORY = new ClassPathXmlApplicationContext("servlet-context.xml");
    private static final BeanFactory BEAN_FACTORY = new AnnotationConfigApplicationContext(SpringConfig.class);

    @Test
    public void getAll() throws Exception {
        UserDAO userDAO = (UserDAO) BEAN_FACTORY.getBean("userDAO");
        userDAO.getAll().stream().map(User::toJsonString).forEach(System.out::println);
    }

}
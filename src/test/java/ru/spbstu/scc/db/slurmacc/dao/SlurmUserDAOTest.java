package ru.spbstu.scc.db.slurmacc.dao;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.spbstu.scc.configs.SpringConfig;

import static org.junit.Assert.*;

/**
 * Created by cloudjumper on 4/21/17.
 */
@Ignore
public class SlurmUserDAOTest {
    @Autowired
    private SlurmUserDAO slurmUserDAO;

    private static final BeanFactory BEAN_FACTORY = new AnnotationConfigApplicationContext(SpringConfig.class);

    @Test
    public void getInstance() throws Exception {
        System.out.println(BEAN_FACTORY.getBean("slurmUserDAO"));
        System.out.println(slurmUserDAO);
    }

}
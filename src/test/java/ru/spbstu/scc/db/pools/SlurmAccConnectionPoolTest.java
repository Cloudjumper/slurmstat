package ru.spbstu.scc.db.pools;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.spbstu.scc.configs.SpringConfig;
import ru.spbstu.scc.db.ConnectionPool;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by cloudjumper on 4/18/17.
 */
@Ignore
public class SlurmAccConnectionPoolTest {

//    private static final BeanFactory BEAN_FACTORY = new ClassPathXmlApplicationContext("servlet-context.xml");
    private static final BeanFactory BEAN_FACTORY = new AnnotationConfigApplicationContext(SpringConfig.class);

    @Test
    public void getInstance() throws Exception {
        ConnectionPool pool1 = (ConnectionPool) BEAN_FACTORY.getBean("slurmAccConnectionPool");
        ConnectionPool pool2 = (ConnectionPool) BEAN_FACTORY.getBean("slurmAccConnectionPool");
        System.out.println(pool1);
        System.out.println(pool2);
        assertThat(pool1.equals(pool2), is(true));
    }

    @Test
    public void getConnection() throws Exception {

    }

}
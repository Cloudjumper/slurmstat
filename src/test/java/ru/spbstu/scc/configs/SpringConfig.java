package ru.spbstu.scc.configs;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.*;
import org.springframework.web.context.WebApplicationContext;
import ru.spbstu.scc.db.ConnectionPool;
import ru.spbstu.scc.db.slurm.dao.AcctTableDAO;
import ru.spbstu.scc.db.slurm.dao.RscJobTableDAO;
import ru.spbstu.scc.db.slurm.dao.UserTableDAO;
import ru.spbstu.scc.db.slurm.pool.SlurmConnectionPool;
import ru.spbstu.scc.db.slurmacc.dao.PartitionDAO;
import ru.spbstu.scc.db.slurmacc.dao.SlurmAccDAO;
import ru.spbstu.scc.db.slurmacc.dao.SlurmUserDAO;
import ru.spbstu.scc.db.slurmacc.dao.UserDAO;
import ru.spbstu.scc.db.slurmacc.pool.SlurmAccConnectionPool;
import ru.spbstu.scc.services.special.CrossDbService;
import ru.spbstu.scc.session.SessionData;

@Configuration
@ComponentScan(basePackages = "ru.spbstu.scc")
public class SpringConfig {

    @Bean
    @Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public SessionData sessionData(){
        return new SessionData();
    }

    @Bean(name = "FASTD")
    @Scope(scopeName = "singleton")
    public Logger logger(){
        return LogManager.getLogger("FASTD");
    }

    // ---------------------------------------------------------------------------

    @Bean
    public SlurmAccConnectionPool slurmAccConnectionPool(){
        return SlurmAccConnectionPool.getInstance();
    }

    @Bean
    public UserDAO userDAO(@Qualifier("slurmAccConnectionPool") ConnectionPool connectionPool){
        return UserDAO.getInstance(connectionPool);
    }

    @Bean
    public SlurmUserDAO slurmUserDAO(@Qualifier("slurmAccConnectionPool") ConnectionPool connectionPool){
        return SlurmUserDAO.getInstance(connectionPool);
    }

    @Bean
    public SlurmAccDAO slurmAccDAO(@Qualifier("slurmAccConnectionPool") ConnectionPool connectionPool){
        return SlurmAccDAO.getInstance(connectionPool);
    }

    @Bean
    public PartitionDAO partitionDAO(@Qualifier("slurmAccConnectionPool") ConnectionPool connectionPool){
        return PartitionDAO.getInstance(connectionPool);
    }

//    @Bean
//    public SlurmAccService slurmAccService(SlurmAccDAO slurmAccDAO){
//        return SlurmAccService.getInstance(slurmAccDAO);
//    }

    // ---------------------------------------------------------------------------

    @Bean
    public SlurmConnectionPool slurmConnectionPool(){
        return SlurmConnectionPool.getInstance();
    }

    @Bean
    public UserTableDAO userTableDAO(@Qualifier("slurmConnectionPool") ConnectionPool connectionPool){
        return UserTableDAO.getInstance(connectionPool);
    }

    @Bean
    public RscJobTableDAO rscJobTableDAO(@Qualifier("slurmConnectionPool") ConnectionPool connectionPool){
        return RscJobTableDAO.getInstance(connectionPool);
    }

    @Bean
    public AcctTableDAO acctTableDAO(@Qualifier("slurmConnectionPool") ConnectionPool connectionPool){
        return AcctTableDAO.getInstance(connectionPool);
    }

    // ---------------------------------------------------------------------------


    @Bean
    public CrossDbService crossDbService(){
        return CrossDbService.getInstance();
    }
}

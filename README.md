# README #

## How to build ##
```bash
mvn clean clean:clean lesscss:compile exec:exec package
```
But you need less compiler, prefixer and correct prefixer.sh in WEB-INF/prefixer.sh

## How to build without frontend (less, prefixer and ect.) ##
```bash
mvn clean install -P no-less-compile
```
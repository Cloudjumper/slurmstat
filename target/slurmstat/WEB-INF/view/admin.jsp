<%--
  Created by IntelliJ IDEA.
  User: cloudjumper
  Date: 2/17/17
  Time: 10:04 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="lang" scope="session" value="${not empty sessionScope.lang ? sessionScope.lang : 'ru'}"/>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="localization" var="bundle"/>
<t:base>
    <jsp:attribute name="title">
        Administrator
    </jsp:attribute>
    <jsp:attribute name="headerMenu">
        <div id="title-menu">
            <div class="header-menu">
                <div>
                    <fmt:message var="administration" bundle="${bundle}" key="web.menu.administration"/>
                    <input id="button-user-actions" class="button-menu" type="button" value="${administration}">
                </div>
                <div>
                    <fmt:message var="userInfo" bundle="${bundle}" key="web.menu.userInfo"/>
                    <input id="button-user-statistic" class="button-menu" type="button" value="${userInfo}">
                </div>
                <div>
                    <fmt:message var="accountInfo" bundle="${bundle}" key="web.menu.accountInfo"/>
                    <input id="button-account-statistic" class="button-menu" type="button" value="${accountInfo}">
                </div>
                <div>
                    <fmt:message var="partitionInfo" bundle="${bundle}" key="web.menu.partitionInfo"/>
                    <input id="button-partition-statistic" class="button-menu" type="button" value="${partitionInfo}">
                </div>
            </div>
        </div>
    </jsp:attribute>
    <jsp:attribute name="extraJs">
        <script type="application/javascript" src="<c:url context="/" value="/static/js/admin.js"/>"></script>
        <script type="application/javascript" src="<c:url context="/" value="/static/js/datetimepicker.js"/>"></script>
        <script type="application/javascript" src="<c:url context="/" value="/static/js/datetimepicker.init.js"/>"></script>
    </jsp:attribute>
    <jsp:attribute name="extraCss">
        <link rel="stylesheet" type="text/css" href="<c:url context="/" value="/static/css/admin.css"/>"/>
        <link rel="stylesheet" type="text/css" href="<c:url context="/" value="/static/css/datetimepicker.css"/>">
    </jsp:attribute>
    <jsp:attribute name="content">
        <%--<jsp:useBean id="all" scope="request" type="java.util.List<ru.spbstu.scc.db.slurmacc.entities.User>"/>--%>
        <div id="user-actions" class="content-block">
            <div id="user-action-title-c" class="title">
                <fmt:message bundle="${bundle}" key="web.admin.administration.title1"/>
            </div>
            <div id="user-action-title-a" class="title" style="display: none">
                <fmt:message bundle="${bundle}" key="web.admin.administration.title2"/>
            </div>
            <div id="user-actions-inputs">
                <div>
                    <fmt:message var="adminLogin" bundle="${bundle}" key="web.admin.administration.login"/>
                    <div>
                        <input style="display: none" autocomplete="false" id="user-set-login" class="text-all text-admin" type="text" placeholder="${adminLogin}">
                        <select autocomplete="false" id="user-get-login" class="select-all select-admin" title="${adminLogin}">
                            <c:forEach items="${all}" var="item">
                                <option>${item.login}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <fmt:message var="adminPass" bundle="${bundle}" key="web.admin.administration.pass"/>
                    <div>
                        <input autocomplete="false" id="user-pass" class="text-all text-admin" type="password" placeholder="${adminPass}">
                    </div>
                    <fmt:message var="adminUserId" bundle="${bundle}" key="web.admin.administration.userId"/>
                    <div>
                        <input autocomplete="false" id="user-user-id" class="text-all text-admin" type="text" placeholder="${adminUserId}">
                    </div>
                    <fmt:message var="adminAccountId" bundle="${bundle}" key="web.admin.administration.accountId"/>
                    <div>
                        <input autocomplete="false" id="user-account-id" class="text-all text-admin" type="text" placeholder="${adminAccountId}">
                    </div>
                    <fmt:message var="adminUser" bundle="${bundle}" key="web.admin.administration.user"/>
                    <div>
                        <input autocomplete="false" id="user-user" class="text-all text-admin" type="text" placeholder="${adminUser}">
                    </div>
                    <fmt:message var="adminAccount" bundle="${bundle}" key="web.admin.administration.account"/>
                    <div>
                        <input autocomplete="false" id="user-account" class="text-all text-admin" type="text" placeholder="${adminAccount}">
                    </div>
                    <fmt:message var="adminRole" bundle="${bundle}" key="web.admin.administration.role"/>
                    <div>
                        <input autocomplete="false" id="user-role" class="text-all text-admin" type="text" placeholder="${adminRole}">
                    </div>
                </div>
                <div>
                    <fmt:message var="adminGetUser" bundle="${bundle}" key="web.admin.administration.getUser"/>
                    <div>
                        <input id="user-action-get" class="button-all button-admin" type="button" value="${adminGetUser}">
                    </div>
                    <fmt:message var="adminDeleteUser" bundle="${bundle}" key="web.admin.administration.deleteUser"/>
                    <div>
                        <input id="user-action-del" class="button-all button-admin" type="button" value="${adminDeleteUser}">
                    </div>
                    <fmt:message var="adminSubmitChanges" bundle="${bundle}" key="web.admin.administration.submitChanges"/>
                    <fmt:message var="adminAddUser" bundle="${bundle}" key="web.admin.administration.addUser"/>
                    <div>
                        <input id="user-action-submit" class="button-all button-admin" type="button" value="${adminSubmitChanges}">
                        <input id="user-action-add" class="button-all button-admin" type="button" value="${adminAddUser}" style="display: none">
                    </div>
                    <%--<fmt:message var="adminSwitch" bundle="${bundle}" key="web.admin.administration.switch"/>--%>
                    <%--<div>--%>
                        <%--<input id="user-action-switch" class="button-all button-admin" type="button" value="${adminSwitch}">--%>
                    <%--</div>--%>
                    <div>
                        <button id="user-action-switch" class="button-all button-admin">
                            <img src="<c:url context="/" value="/static/img/swap-horizontal-orientation-arrows.png"/>">
                        </button>
                    </div>
                    <div>
                        <button id="user-action-help" class="button-all button-admin">
                            <img src="<c:url context="/" value="/static/img/help_outline_white_54x54.png"/>">
                        </button>
                    </div>
                </div>
                <div>
                    <fmt:message var="adminHelp" bundle="${bundle}" key="web.admin.administration.help"/>
                    <div id="user-help">
                        ${adminHelp}
                    </div>
                </div>
                <div>
                    <div id="user-error">
                        some err
                    </div>
                </div>
            </div>
        </div>
        <div id="user-statistic" class="content-block">
            <div class="title">
                <fmt:message bundle="${bundle}" key="web.common.userStat.title"/>
            </div>
            <div>
                <div id="user-statistic-actions">
                    <div>
                        <%--<input autocomplete="false" id="user-statistic-user" class="text-all text-admin" type="text" placeholder="User. Not a login!">--%>
                        <select style="width: 100%" autocomplete="false" id="user-statistic-user" class="select-all select-admin" title="Users">
                            <c:forEach items="${slurmUsers}" var="item">
                                <option>${item.user} (${item.userId})</option>
                            </c:forEach>
                        </select>
                    </div>
                    <fmt:message var="startTime" bundle="${bundle}" key="web.common.startTime"/>
                    <div>
                        <input autocomplete="false" id="user-statistic-stime" class="datetimepicker-stime text-all text-admin" type="text" placeholder="${startTime}">
                    </div>
                    <fmt:message var="endTime" bundle="${bundle}" key="web.common.endTime"/>
                    <div>
                        <input autocomplete="false" id="user-statistic-etime" class="datetimepicker-etime text-all text-admin" type="text" placeholder="${endTime}">
                    </div>
                    <fmt:message var="stat" bundle="${bundle}" key="web.common.getStat"/>
                    <div>
                        <input id="user-statistic-submit" class="button-all button-admin" type="button" value="${stat}">
                    </div>
                </div>
                <div id="user-statistic-results">
                    <table>
                        <tr>
                            <th><fmt:message bundle="${bundle}" key="web.common.partition"/></th>
                            <th><fmt:message bundle="${bundle}" key="web.common.user"/></th>
                            <th><fmt:message bundle="${bundle}" key="web.common.account"/></th>
                            <th><fmt:message bundle="${bundle}" key="web.common.workTime"/></th>
                        </tr>
                    </table>
                </div>
            </div>
            <div id="user-statistic-error">

            </div>
        </div>
        <div id="account-statistic" class="content-block">
            <div class="title">
                <fmt:message bundle="${bundle}" key="web.common.accountStat.title"/>
            </div>
            <div>
                <div id="account-statistic-actions">
                    <div>
                        <select style="width: 100%" autocomplete="false" id="account-statistic-account" class="select-all select-admin" title="Account">
                            <c:forEach items="${slurmAccounts}" var="item">
                                <option>${item}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <fmt:message var="startTime" bundle="${bundle}" key="web.common.startTime"/>
                    <div>
                        <input autocomplete="false" id="account-statistic-stime" class="datetimepicker-stime text-all text-admin" type="text" placeholder="${startTime}">
                    </div>
                    <fmt:message var="endTime" bundle="${bundle}" key="web.common.endTime"/>
                    <div>
                        <input autocomplete="false" id="account-statistic-etime" class="datetimepicker-etime text-all text-admin" type="text" placeholder="${endTime}">
                    </div>
                    <fmt:message var="stat" bundle="${bundle}" key="web.common.getStat"/>
                    <div>
                        <input id="account-statistic-submit" class="button-all button-admin" type="button" value="${stat}">
                    </div>
                </div>
                <div id="account-statistic-results">
                    <table>
                        <tr>
                            <th><fmt:message bundle="${bundle}" key="web.common.partition"/></th>
                            <th><fmt:message bundle="${bundle}" key="web.common.user"/></th>
                            <th><fmt:message bundle="${bundle}" key="web.common.account"/></th>
                            <th><fmt:message bundle="${bundle}" key="web.common.workTime"/></th>
                        </tr>
                    </table>
                </div>
            </div>
            <div id="account-statistic-error"></div>
        </div>
        <div id="partition-statistic" class="content-block">
            <div class="title">
                <fmt:message bundle="${bundle}" key="web.common.partitionStat.title"/>
            </div>
            <div>
                <div id="partition-statistic-actions">
                    <div>
                        <select style="width: 100%" autocomplete="false" id="partition-statistic-partition" class="select-all select-admin" title="Account">
                            <c:forEach items="${slurmPartitions}" var="item">
                                <option>${item}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <fmt:message var="startTime" bundle="${bundle}" key="web.common.startTime"/>
                    <div>
                        <input autocomplete="false" id="partition-statistic-stime" class="datetimepicker-stime text-all text-admin" type="text" placeholder="${startTime}">
                    </div>
                    <fmt:message var="endTime" bundle="${bundle}" key="web.common.endTime"/>
                    <div>
                        <input autocomplete="false" id="partition-statistic-etime" class="datetimepicker-etime text-all text-admin" type="text" placeholder="${endTime}">
                    </div>
                    <fmt:message var="stat" bundle="${bundle}" key="web.common.getStat"/>
                    <div>
                        <input id="partition-statistic-submit" class="button-all button-admin" type="button" value="${stat}">
                    </div>
                </div>
                <div id="partition-statistic-results">
                    <table>
                        <tr>
                            <th><fmt:message bundle="${bundle}" key="web.common.account"/>&#8645;</th>
                            <th><fmt:message bundle="${bundle}" key="web.common.workTime"/>&#8645;</th>
                            <th><fmt:message bundle="${bundle}" key="web.common.partWorkTime"/>&#8645;</th>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <th><fmt:message bundle="${bundle}" key="web.common.maxWorkTime"/></th>
                            <th><fmt:message bundle="${bundle}" key="web.common.sumWorkTime"/></th>
                            <th><fmt:message bundle="${bundle}" key="web.common.load"/></th>
                        </tr>
                    </table>
                </div>
            </div>
            <div id="partition-statistic-error"></div>
        </div>
        <div class="content-block">
            <div class="title">
                <fmt:message bundle="${bundle}" key="web.admin.common.userTable.title"/>
            </div>
            <div id="sum-table">
                <table>
                    <tr>
                        <th><fmt:message bundle="${bundle}" key="web.admin.common.userTable.login"/></th>
                        <th><fmt:message bundle="${bundle}" key="web.admin.common.userTable.user"/></th>
                        <th><fmt:message bundle="${bundle}" key="web.admin.common.userTable.userId"/></th>
                        <th><fmt:message bundle="${bundle}" key="web.admin.common.userTable.account"/></th>
                        <th><fmt:message bundle="${bundle}" key="web.admin.common.userTable.accountId"/></th>
                        <th><fmt:message bundle="${bundle}" key="web.admin.common.userTable.role"/></th>
                    </tr>
                    <c:forEach items="${all}" var="item">
                    <tr>
                        <td>${item.login}</td>
                        <td>${item.user}</td>
                        <td>${item.userId}</td>
                        <td>${item.account}</td>
                        <td>${item.accountId}</td>
                        <td>${item.roleCode}</td>
                    </tr>
                </c:forEach>
                </table>
            </div>
        </div>
    </jsp:attribute>
</t:base>

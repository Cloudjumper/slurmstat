var dict;
var lang;
var store;

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    data:{
        '_csrf': $("#_csrf").val()
    },
    beforeSend: function (xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRF-TOKEN", $("#_csrf").val());
        }
    }
});

function supports_html5_storage() {
    try {
        return 'localStorage' in window && window['localStorage'] !== null;
    } catch (e) {
        return false;
    }
}

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}

function getLang() {
    if (store && localStorage['lang'] != null)
        lang = localStorage['lang'];
    else if (!store && getCookie('lang') != 'undefined')
        lang = getCookie('lang');
    else lang = 'ru';
}

function setLang(l) {
    if (store)
        lang = localStorage['lang'] = l;
    else if (!store){
        setCookie("lang", l);
        lang = getCookie('lang');
    }
}

function logIn() {
    $("#loader").show();
    const login = $('#login-login-h').val();
    const pass = $('#login-pass-h').val();
    $.ajax({
        url: "/login",
        type: 'POST',
        dataType: 'json',
        data: {
            'username': login,
            'password': sha1(pass)
            // '_csrf': $("#_csrf").val()
        },
        success: function(data, textStatus){
            $("#loader").hide();
            console.log(data);
            console.log(data["redirect"]);
            if (data["redirect"])
                location.href = data["redirect"];
            else
                location.href = "/";
        },
        error: function(response, status, error){
            $("#loader").hide();
            $("#login-err-h").text(response["responseJSON"]["error"]);
            $("#login-err-h").show("fade", 200);
        }
    });
}

$(document).ready(function () {
    store = true;
    if (!supports_html5_storage()){
        console.warn("Unsupported local storage");
        store = false;
    }

    getLang();

    $.ajax({
        url: "/api/lang",
        type: 'GET',
        data: {},
        dataType: "json",
        success: function(response){
            dict = response;
        },
        error: function(response, status, error){
            console.error("Error in downloading localization")
        }
    });

    // $.ajax({
    //     url: "/api/lang",
    //     type: 'GET',
    //     data: {},
    //     success: function(response){
    //         localStorage['dict'] = response;
    //         if (localStorage['lang'] == null)
    //             localStorage['lang'] = "ru"
    //     },
    //     error: function(response, status, error){
    //         console.error("Error in downloading localization")
    //     }
    // });
});

$(".button-login-menu").click(function () {
    $(".login-form").toggle("fade", 200);
});

$(".key-login").on("keypress", function (e) {
    if(e.keyCode == 13){
        logIn();
    }
});

$("#login-btn-h").click(function () {
    logIn();
});

$("#logout-btn-h").click(function () {
    $.ajax({
        url: "/logout",
        type: 'DELETE',
        data: {},
        success: function(response){
            location.href = response["response"];
        },
        error: function(response, status, error){
            console.log(response.responseText)
        }
    });
});

$(".lang-btn").click(function () {
    var localLang = $(this).attr("l");
    $.ajax({
        url: "/api/lang",
        type: 'POST',
        data: {
            lang: localLang
        },
        statusCode:{
            304: function () {
            },
            500: function (response) {
                console.log(response["error"])
            },
            200: function () {
                location.reload();
            }
        },
        error: function(response, status, error){
        }
    });
});


/**
 * Created by cloudjumper on 2/26/17.
 */
$(document).ready(function () {
    var today = new Date();
    var past = new Date(today).setMonth(today.getMonth() - 1);

    $('.datetimepicker-stime').datetimepicker({
        format:'d-m-Y H:i',
        defaultTime: '00:00',
        defaultDate: past,
        lang:'ru',
        // inline: true
        // formatTime:'h:mm',
        // formatDate:'DD.MM.YYYY'
    });
    $('.datetimepicker-etime').datetimepicker({
        format:'d-m-Y H:i',
        defaultTime: '00:00',
        defaultDate: today,
        lang:'ru',
        // inline: true
        // formatTime:'h:mm',
        // formatDate:'DD.MM.YYYY'
    });
    $.datetimepicker.setLocale('ru');

    // $(function(){
    //     $('#date_timepicker_start').datetimepicker({
    //         format:'Y/m/d',
    //         onShow:function( ct ){
    //             this.setOptions({
    //                 maxDate:$('#date_timepicker_end').val()?$('#date_timepicker_end').val():false
    //             })
    //         },
    //         timepicker:false
    //     });
    //     $('#date_timepicker_end').datetimepicker({
    //         format:'Y/m/d',
    //         onShow:function( ct ){
    //             this.setOptions({
    //                 minDate:$('#date_timepicker_start').val()?$('#date_timepicker_start').val():false
    //             })
    //         },
    //         timepicker:false
    //     });
    // });
});

function setDefaultDate() {

}